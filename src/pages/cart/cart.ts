import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Events, Platform, ToastController, LoadingController } from 'ionic-angular';
import { OrderPage } from '../order/order';
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { HTTP } from '@ionic-native/http';
import { EmailComposer } from '@ionic-native/email-composer';

import { HomePage } from '../home/home';
import { Global } from '../../app/global';
import { ConnectionVariables } from '../../app/connectionVariables';
import { Encryption } from '../../providers/cryptprovider';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html'
})
export class CartPage {

  items: Array<String>;
  orderPage: OrderPage;
  cartsize: number;

  public deviceWidth = this.platform.width();

  // Array that is copied from the Whole array, and is updated upon search within this page
  orderList = Array<Array<String>>();
  // Actual array that reads from storage only once
  orderListWhole = Array<Array<String>>();
  // Maintain temporary list of entries sent to server
  extractedItemTS = Array<String>();

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public storage: Storage, 
    public alertCtrl: AlertController,
    private sqlite: SQLite,
    public platform: Platform,
    public network: Network,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public http: HTTP,
    public crypt: Encryption,
    public emailComposer: EmailComposer, 
    public events: Events) {

    this.setItems()
    .then(() => {
      // Update cart size to size set before previous Checkout initiation
      storage.get('cartsize')
      .then((val) => {
        this.cartsize = Number(val.toString());
        console.log('Local value for cartsize updated to ' + this.cartsize + '.');
        // Check if checkout process is in progress
        storage.get('checkoutInProgress')
        .then((val) => {
          if(val == 'true') {
            console.log('Resuming checkout progress.');
            this.checkoutProcess();
          }
          else {
            console.log('Checkout not in progress.');
          }
        })
        .catch((e) => {
          console.log('Error in reading checkoutInProgress from storage.');
        });
      })
      .catch((e) => {
        console.log('Error in fetching original cart size.');
      });
    })
    .catch((e) => {
      console.log('Unable to set items.');
      console.log(e);
    });
    
  }

  ngOnInit() {

  }

  addMore() {

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });
    loading.present();

    this.pageRedirect('order', '', 'none', 0, 200, loading);

  }

  getTop20EntriesAsSQL() {

    console.log('CHKOUT: Call to getTop20EntriesAsSQL.');
    let backend_sql = '{';
    return new Promise((resolve, reject) => {
        this.sqlite.create({
            name: Global.db,
            location: 'default'
        })
        .then((db: SQLiteObject) => {
            let sql = 'SELECT * from orderTable ' +
                'WHERE submitted = 0 ' +
                'AND user_id = ? ' +
                'ORDER BY ponumber, shiptoid, timestamp ' +
                'LIMIT 20';
                console.log('CHKOUT: Initiate fetching rows from SQLite.');
            db.executeSql(sql, [Global.userid])
            .then((result) => {
              if(result.rows.length == 0) {
                reject('CHKOUT: ERROR in getTop20EntriesAsSQL -> No rows found.');
              }
              else {
                console.log('CHKOUT: Building string of paramset.');
                for(let i = 0; i < result.rows.length; i++) {
                  this.extractedItemTS.push(result.rows.item(i).timestamp.toString());

                  // Build string containing parameters (equivalient to outcome of stringify on JSON object)
                  let paramset = '{' +
                    '"status": "0", ' +
                    '"ponumber": "' + result.rows.item(i).ponumber + '", ' +
                    '"scannedby": "' + Global.userid + '", ' +
                    '"qty": "' + result.rows.item(i).quantity + '", ' +
                    '"tagid": "' + result.rows.item(i).tagid + '", ' +
                    '"shiptoid": "' + result.rows.item(i).shiptoid + '", ' +
                    '"contactid": "' + result.rows.item(i).contactid + '", ' +
                    '"extdesc": "' + result.rows.item(i).extdesc + '", ' +
                    '"createdby": "' + Global.userid + '"' +
                    '}';
                  
                  // Encrypt each record (since encryption of long strings is not working)
                  this.crypt.encryptDataAsPromise(paramset)
                  .then((param_enc) => {
                    let entry = '"' + (i+1) + '": "' + param_enc.toString() + '"';
                    return entry;
                  })
                  .then((entry) => {
                    if(i!= 0) {
                      backend_sql += ', ' + entry;
                    }
                    else {
                      backend_sql += entry;
                    }
                  })
                  .then(() => {
                    if(i == (result.rows.length - 1)) {
                      console.log('CHKOUT: Completed building param string.');
                      resolve(backend_sql + '}');
                    }
                  })
                  .catch((e) => {
                    console.log(e);
                  });
                }
              }
            })
            .catch((e) => {
                reject(e);
            });
        })
        .catch((e) => {
            reject(e);
        });
    });

  }

  updateOrderTable(cartSize: number) {

    console.log('CHKOUT: Call to updateOrderTable.');
    // Return cart size after decrement
    let count = 0;
    return new Promise((resolve, reject) => {
        this.sqlite.create({
            name: Global.db,
            location: 'default'
        })
        .then((db: SQLiteObject) => {
            for(let i = 0; i < this.extractedItemTS.length; i++) {
                let sql = 'UPDATE orderTable ' +
                    'SET submitted = 1 ' +
                    'WHERE timestamp = ? ' +
                    'AND user_id = ?';
                console.log('CHKOUT: Initiate update rows in SQLite.');
                db.executeSql(sql, [this.extractedItemTS[i], Global.userid])
                .then(() => {
                    cartSize--;
                    count++;
                    console.log('CHKOUT: STATUS in updateOrderTable -> Decremented cart size by 1. Now contains ' + cartSize + ' items.');
                    if(i == (this.extractedItemTS.length - 1)) {
                        console.log('Updated ' + count + ' entries.');
                        resolve(cartSize);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
            }
        })
        .catch((e) => {
            reject(e);
        });
    });
  }

  updateOrderList(numberOfItems: number) {

    console.log('CHKOUT: Call to updateOrderList.');
    return new Promise((resolve, reject) => {
        this.sqlite.create({
            name: Global.db,
            location: 'default'
        })
        .then((db: SQLiteObject) => {
            console.log('CHKOUT: Inserting entry of ' + numberOfItems + ' items.');
            let timestamp = new Date();
            let sql = 'INSERT INTO orderList(timestamp, size, user_id) ' +
                'VALUES (?, ?, ?)';
            console.log('CHKOUT: Initiate update rows in SQLite.');
            db.executeSql(sql, [timestamp, numberOfItems, Global.userid])
            .then(() => {
                resolve('Success');
            })
            .catch((e) => {
                reject(e);
            });
        })
        .catch((e) => {
            reject(e);
        });
    });
  }

  checkoutProcess() {

    console.log('CHKOUT: In Checkout process.');
    // Proceed only if network is available
    let isOnline = this.network.type;
    // Check if network is available
    if(isOnline == 'none') {
      // No network connection
      console.log('CHKOUT: ERROR -> Internet unavailable to proceed.');
      let toast = this.toastCtrl.create({
        message: 'No internet connection available. Try again later.',
        showCloseButton: true,
        closeButtonText: 'OK',
        position: 'bottom'
      });
      toast.present();
      return;
    }
    else {

      // Network connection available
      let loading = this.loadingCtrl.create({
        content: 'Submitting order...'
      });
      loading.present();

      // Send to server
      /*
      sd1_status
      sd1_po_number     ponumber
      sd1_scanned_on    timestamp
      sd1_scanned_by    userid
      sd1_scanned_qty   quantity
      sd1_item_id       product
      ship_to_id        shiptoid
      created_by        ?userid
      created_on        ?timestamp
      
      // Send as batches
      */

      let cartSize: number = this.orderListWhole.length;
      //alert(cartSize);
      if(this.orderListWhole.length <= 0) {
        console.log('CHKOUT: ERROR -> No items to checkout.');
        this.storage.remove('attemptCount')
        .then(() => {
          this.storage.remove('checkoutInProgress')
          .then(() => {
            console.log('CHKOUT: STATUS Ending exec -> Removed attemptCount & checkoutInProgress entries from storage.');
            this.pageRedirect('home', 'Completed checkout of all possible order items.', 'toast', 2000, 2000, loading);
          })
          .catch((e) => {
            console.log('CHKOUT: A-ERROR -> ' + e);
            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
          });
        })
        .catch((e) => {
          console.log('CHKOUT: A-ERROR -> ' + e);
          this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
        });
      }
      else {
        
        let backend_sql = '';
        
        //console.log('Fetching top 20 entries and creating SQL.');
        this.getTop20EntriesAsSQL()
        .then((result) => {

          isOnline = this.network.type;
          if(isOnline != 'none') {
            // Network available to write to server

            backend_sql += result.toString();
            //let link = 'http://127.0.0.1/airhydrodemo.php';
              let data = {
                'action': 'insert',
                //'contentinfo': 'compressed_lzw',
                //'params': this.crypt.compress(backend_sql)
                'params': backend_sql
              };
              console.log('CHKOUT: Backend request content => ' + backend_sql);
              // console.log('CHKOUT: Backend request content => ' + this.crypt.compress(backend_sql));
              console.log('CHKOUT: Posting fetched rows to server.');
  
              this.http.setRequestTimeout(ConnectionVariables.timeout);
              this.http.acceptAllCerts(true)
              .then(() => {
                this.http.post(ConnectionVariables.httpUrl, data, {})
                .then((response) => {
                  //let resp_parse = JSON.parse(response.data.toString());
                  console.log('CHKOUT: Server response => ' + response.data);
                  this.crypt.decryptDataAsPromise(response.data)
                  .then((decryptedval) => {
                    console.log('CHKOUT: Decrypted => ' + decryptedval.toString());
                    let res = JSON.parse(decryptedval.toString());
                    if(res.status == 1) {
      
                      console.log('CHKOUT: Positive response from server.');
                      this.updateOrderTable(cartSize)
                      .then((newcartsize) => {
                        let itemsCheckedOut = this.cartsize - Number(newcartsize);
                        cartSize = Number(newcartsize);
                        if(cartSize == 0) {
                          // All items checked out
                          console.log('CHKOUT: All items checked out. Inserting into orderList.');
                          this.updateOrderList(itemsCheckedOut)
                          .then((result) => {
                            // Remove 'checkoutInProgress' key-value pair from storage
                            this.storage.remove('checkoutInProgress')
                            .then(() => {
                              console.log('CHKOUT: STATUS -> Removed checkoutInProgress from storage.');
                              this.pageRedirect('home', 'Order checked out successfully!', 'toast', 2000, 2000, loading);
                            })
                            .catch((e) => {
                              console.log('CHKOUT: Error in removing checkoutInProgress from storage.');
                              console.log('CHKOUT: A-ERROR -> ' + e);
                              this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                          })
                          .catch((e) => {
                            console.log('CHKOUT: Error in updating orderList.');
                            console.log('CHKOUT: A-ERROR -> ' + e);
                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                          });
                        }
                        else {
                          // More items remaining in cart
                          console.log('Not all items checked out.');
                          if(this.network.type != 'none') {
                            console.log('Network available to continue.');
                            // Set attemptCount = 0 in storage
                            // Set 'checkoutInProgress: true' key-value pair in storage
                            this.storage.set('attempCount', 0)
                            .then(() => {
                              this.storage.set('checkoutInProgress', 'true')
                              .then(() => {
                                this.pageRedirect('cart', '', 'none', 0, 400, loading);
                              })
                              .catch((e) => {
                                console.log('Error in setting checkoutInProgress in storage.');
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                              });
                            })
                            .catch((e) => {
                              console.log('CHKOUT: Error in setting attemptCount in storage.');
                              console.log('CHKOUT: A-ERROR -> ' + e);
                              this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                          }
                          else {
                            console.log('CHKOUT: STATUS Ending exec -> Network not available to continue checkout.');
                            console.log('CHKOUT: STATUS Ending exec -> Updating orderList.');
                            this.updateOrderList(itemsCheckedOut)
                            .then((result) => {
                              // Remove 'checkoutInProgress' key-value pair from storage
                              this.storage.remove('checkoutInProgress')
                              .then(() => {
                                this.pageRedirect('cart', 'Network issues. Unable to checkout all items at this time.', 'toastok', 2000, 2000, loading);
                              })
                              .catch((e) => {
                                console.log('CHKOUT: Error in removing checkoutInProgress.');
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                              });
                            })
                            .catch((e) => {
                              console.log('CHKOUT: A-ERROR -> ' + e);
                              this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                          }
                        }
                      })
                      .catch((e) => {
                        console.log('CHKOUT: A-ERROR -> ' + e);
                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                      });
                    }
                    else {
                      
                      // Unsuccessful response from server
                      console.log('CHKOUT: Failed response from server.');
                      console.log('CHKOUT: ERROR -> Error ' + res.status + ': ' + res.error);
                      // Read value stored in key 'attemptCount'
                      // Increment attemptCount
                      console.log('CHKOUT: Looking up attempt number for current entries.');
                      this.storage.get('attemptCount')
                      .then((val) => {
                        let attemptCount = Number(val) + 1;
                        this.storage.set('attemptCount', attemptCount)
                        .then(() => {
                          if(attemptCount > 10) {
                            console.log('CHKOUT: ERROR -> Multiple failed attempts.');
                            let itemsCheckedOut = this.cartsize - cartSize;
                            if(itemsCheckedOut > 0) {
                              console.log('CHKOUT: STATUS Ending exec -> Updating info in SQLite of checkout out items.');
                              this.updateOrderList(itemsCheckedOut)
                              .then((result) => {
                                // Remove storage variable 'checkoutInProgress' and redirect to cart page
                                this.storage.remove('checkoutInProgress')
                                .then(() => {
                                  this.storage.remove('attemptCount')
                                  .then(() => {
                                    console.log('CHKOUT: STATUS Ending exec -> Removed storage variables checkoutInProgress, attemptCount.');
                                    this.pageRedirect('cart', 'Multiple failed attempts to write to server. Please try again later.', 'toastok', 2000, 1500, loading);
                                  })
                                  .catch((e) => {
                                    console.log('CHKOUT: ERROR -> Error in resetting attemptCount in storage.');
                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                  });
                                })
                                .catch((e) => {
                                  console.log('CHKOUT: ERROR -> Error in removing checkoutInProgress from storage.');
                                  console.log('CHKOUT: A-ERROR -> ' + e);
                                  this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                });
                              })
                              .catch((e) => {
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                              });
                            }
                            else
                            {
                              this.storage.remove('checkoutInProgress')
                              .then(() => {
                                this.storage.remove('attemptCount')
                                .then(() => {
                                  console.log('CHKOUT: STATUS Ending exec -> Removed storage variables checkoutInProgress, attemptCount.');
                                  this.pageRedirect('cart', 'Multiple failed attempts to write to server. Please try again later.', 'toastok', 2000, 1500, loading);
                                })
                                .catch((e) => {
                                  console.log('CHKOUT: Error in removing attemptCount.');
                                  console.log('CHKOUT: A-ERROR -> ' + e);
                                  this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                });
                              })
                              .catch((e) => {
                                console.log('CHKOUT: Error in removing checkoutInProgress.');
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                              });
                            }
                          }
                          else {
                            console.log('Resending request.');
                            console.log('CHKOUT: Attempting to send data to server again.');
                            // Set 'checkoutInProgress: true' key-value pair in storage
                            this.storage.set('checkoutInProgress', 'true')
                            .then(() => {
                              console.log('CHKOUT: STATUS Retry -> Set checkoutInProgress to true.');
                              this.pageRedirect('cart', '', 'none', 0, 500, loading);
                            })
                            .catch((e) => {
                              console.log('CHKOUT: Error in writing checkoutInProgress variable to storage.');
                              console.log('CHKOUT: A-ERROR -> ' + e);
                              this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                          }
                        })
                        .catch((e) => {
                          console.log('CHKOUT: Error in updating attemptCount.');
                          console.log('CHKOUT: A-ERROR -> ' + e);
                          this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                        });
                      })
                      .catch((e) => {
                        console.log('AttemptCount not found in storage.');
                        console.log(e);
                        this.storage.set('attemptCount', 1)
                        .then(() => {
                          // Set 'checkoutInProgress: true' key-value pair in storage
                          this.storage.set('checkoutInProgress', 'true')
                          .then(() => {
                            console.log('CHKOUT: Attempting to send data to server again.');
                            this.pageRedirect('cart', '', 'none', 0, 500, loading);
                          })
                          .catch((e) => {
                            console.log('CHKOUT: Error in writing checkoutInProgress variable to storage.');
                            console.log('CHKOUT: A-ERROR -> ' + e);
                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                          });
                        })
                        .catch((e) => {
                          console.log('CHKOUT: Error in setting attemptCount in storage.');
                          console.log('CHKOUT: A-ERROR -> ' + e);
                          this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                        });
                      });

                    }
                  })
                  .catch((e) => {
                    console.log('CHKOUT: Error in decrypting data received.');
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                  });
                })
                .catch((e) => {
                  console.log(e);
                  this.storage.remove('checkoutInProgress')
                  .then(() => {
                    this.pageRedirect('cart', 'Issue in request sent to server. Please report incident.', 'toastok', 2000, 1500, loading);
                  })
                  .catch((e) => {
                    console.log('CHKOUT: Error in removing checkoutInProgress.');
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                  });
                });
              })
              .catch((e) => {
                  console.log('CHKOUT: Error in connecting to server.');
                  console.log('CHKOUT: A-ERROR -> ' + e);
                  this.storage.remove('checkoutInProgress')
                  .then(() => {
                    this.pageRedirect('cart', 'Issue in connecting to server. Please report incident.', 'toastok', 2000, 1500, loading);
                  })
                  .catch((e) => {
                    console.log('CHKOUT: Error in removing checkoutInProgress.');
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                  });
              })
              
          }
          else
          {
            console.log('CHKOUT: ERROR -> Fetched records, but no network to continue.');
            // No network connection
            let itemsCheckedOut = this.cartsize - cartSize;
            if(itemsCheckedOut > 0) {
              this.updateOrderList(itemsCheckedOut)
              .then((result) => {
                // Remove storage variable and redirect to cart page
                this.storage.remove('checkoutInProgress')
                .then(() => {
                  console.log('CHKOUT: STATUS Ending exec -> Removed checkoutInProgress from storage.');
                  this.pageRedirect('cart', 'Network error. Unable to proceed at this point of time.', 'toastok', 0, 500, loading);
                })
                .catch((e) => {
                  console.log('CHKOUT: Error in removing checkoutInProgress.');
                  console.log('CHKOUT: A-ERROR -> ' + e);
                  this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                });
              })
              .catch((e) => {
                console.log('CHKOUT: A-ERROR -> ' + e);
                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
              });
            }
            else {
              console.log('CHKOUT: STATUS Ending exec -> No checked out items to update in SQLite.');
              this.storage.remove('checkoutInProgress')
              .then(() => {
               console.log('CHKOUT: STATUS Ending exec -> Removed checkoutInProgress from storage.');
                this.pageRedirect('cart', 'Network error. Unable to proceed at this point of time.', 'toastok', 0, 2000, loading);
              })
              .catch((e) => {
                console.log('CHKOUT: Error in removing checkoutInProgress.');
                console.log('CHKOUT: A-ERROR -> ' + e);
                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
              });
            }
          }
        })
        .catch((e) => {
          console.log('CHKOUT: STATUS Ending exec -> Issue in fetching top 20 entries.');
          console.log('CHKOUT: A-ERROR -> ' + e);
          this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
        });
      }
    }

  }

  getCartSize() {

    console.log('GETSZE: Call to getCartSize.');
    return new Promise((resolve, reject) => {
        this.sqlite.create({
            name: Global.db,
            location: 'default'
        })
        .then((db: SQLiteObject) => {
            let sql = 'SELECT COUNT(timestamp) as itemsincart FROM orderTable ' +
                'WHERE submitted = 0 ' +
                'AND user_id = ?';
            console.log('GETSZE: Initiate count items in cart from SQLite.');
            db.executeSql(sql, [Global.userid])
            .then((result) => {
                this.storage.set('cartsize', Number(result.rows.item(0).itemsincart))
                .then(() => {
                    console.log('GETSZE: STATUS Ending exec -> Successfully updated.');
                    resolve('Success');
                })
                .catch((e) => {
                    reject(e);
                });
            })
            .catch((e) => {
                reject(e);
            });
        })
        .catch((e) => {
            reject(e);
        });
    });
  }

  checkoutCart() {

    this.getCartSize()
    .then(() => {
      this.storage.get('cartsize')
      .then((val) => {
        this.cartsize = Number(val);
        console.log('Local value for cartsize updated to ' + this.cartsize + '.');
        setTimeout(() => {
          this.checkoutProcess();
        }, 100);
      })
      .catch((e) => {
        console.log('Error in fetching original cart size.');
      });
      
    })
    .catch((e) => {
      console.log('Cannot proceed, issue with initiating cart size.');
    });
    
  }

  confirmCheckout(canProceed: boolean) {

    if(!canProceed) {
      // Show toast about empty cart
      let toast = this.toastCtrl.create({
        message: 'You do not have any items in your cart!',
        duration: 1500,
        position: 'middle'
      });
      toast.present();
    }
    else {

      let alertConfirm = this.alertCtrl.create({
        title: 'Submit Order',
        message: 'Do you want to submit order?',
        buttons: [
          {
            text: 'No',
            role: 'cancel'
          },
          {
            text: 'Yes',
            handler: () => {
              this.checkoutCart();
            }
          }
        ]
      });
      alertConfirm.present();

    }

  }

  getRecordsAsMailBody(platform: string) {
    console.log('CHKOUT: Call to getRecordsAsMailBody');
    //let body = "<h3>ORDER CONTENTS</h3><br>";
    
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: Global.db,
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM orderTable ' +
          'WHERE submitted = 0 ' +
          'AND user_id = ? ' +
          'ORDER BY ponumber, timestamp';
        console.log('CHKOUT: Initiate fetching rows from SQLite');
        db.executeSql(sql, [Global.userid])
        .then((result) => {
          if(result.rows.length == 0) {
            reject('CHKOUT: ERROR in getRecordsAsMailBody -> No rows found.');
          }
          else {
            console.log('CHKOUT: Building email body.');
  
            if(platform == 'android') {
              let body = "ORDER CONTENTS\r\n\r\n" +
                "Order created by " + Global.username + "\r\n\r\n";
              // List form - only Android
              for(let i = 0; i < result.rows.length; i++) {
                body += "Item " + (i+1) + "\r\n" +
                  "Ship to ID: " + result.rows.item(i).shiptoid + "\r\n" +
                  "Contact ID: " + result.rows.item(i).contactid + "\r\n" +
                  "PO Number: " + result.rows.item(i).ponumber + "\r\n" +
                  "Tag ID: " + result.rows.item(i).tagid + "\r\n" +
                  "Quantity: " + result.rows.item(i).quantity + "\r\n" +
                  "Extended descr.: " + result.rows.item(i).extdesc + "\r\n\r\n";
                if(i == (result.rows.length - 1)) {
                  console.log('CHKOUT: Completed building email body.');
                  resolve(body);
                }
              }
            }
            
            else if(platform == 'ios') {
              // Tabular form - only iOS
              let body = "<h3>ORDER CONTENTS</h3><br> " +
                "<p>Order created by: <b>" + Global.username + "</b><br><br><p>" +
                "<table style=\"width: 100%; border-width: 2px; border-style: solid; border-color: #000000; border-collapse: collapse;\"> " +
                "<tr> " +
                " <td style=\"width: 7%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>#</b></td> " +
                " <td style=\"width: 20%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Ship to ID</b></td> " +
                " <td style=\"width: 15%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Contact ID</b></td> " +
                " <td style=\"width: 20%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>PO Number</b></td> " +
                " <td style=\"width: 25%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Tag ID</b></td> " +
                " <td style=\"width: 13%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Quantity</b></td> " +
                "</tr> ";
              for(let i = 0; i < result.rows.length; i++) {
                body += "<tr> " +
                  " <td rowspan=\"2\" style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; text-align: center; border-width: 1px; border-style:solid; border-color: #696969;\">" + (i+1) + "</td>" +
                  " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).shiptoid + "</td>" +
                  " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).contactid + "</td>" +
                  " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).ponumber + "</td>" +
                  " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).tagid + "</td>" +
                  " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).quantity + "</td>" +
                  " </tr>" +
                  "<tr>" +
                  " <td colspan=\"5\" style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969;\"> Ext desc: " + result.rows.item(i).extdesc + "</td>" +
                  "</tr>";
                if(i == (result.rows.length - 1)) {
                  console.log('CHKOUT: Completed building email body.');
                  resolve(body + "</table>");
                }
              }
            }

          }
        })
        .catch((e) => {
          reject(e);
        });
      })
      .catch((e) => {
        reject(e);
      });
    });
  }

  sendAsMail(platform: string) {

    let loading = this.loadingCtrl.create({
      content: 'Sending as email...'
    });
    loading.present();

    /*
    this.emailComposer.isAvailable()
    .then((available: boolean) => {
      if(available) {
        */
        console.log('CHKOUT: Email client unavailable.');
        this.getRecordsAsMailBody(platform)
        .then((body) => {

          let email;

          if(platform == 'android') {
            email = {
              subject: 'Air Hydro Items Order',
              body: body.toString(),
              isHtml: false
            };
          }
          else if(platform == 'ios') {
            email = {
              //to: '',
              subject: 'Air Hydro Items Order',
              body: body.toString(),
              isHtml: true
            };
          }
  
          setTimeout(() => {

            this.emailComposer.open(email)
            .then(() => {
              console.log('CHKOUT: Email client opened.');
              loading.dismiss();

              // Order sent as email
              //this.pageRedirect('cart', 'Items checked out as email', 'toast', 1500, 500, loading);

            })
            .catch((e) => {
              console.log('CHKOUT: A-ERROR -> ' + e);
              console.log('CHKOUT: ERROR -> Unable to open email client.');
              loading.dismiss();
            });

          }, 200)
          
        })
        .catch((e) => {
          console.log('CHKOUT: A-ERROR -> ' + e);
          console.log('CHKOUT: ERROR -> Issue in getting rows as email body.');
          loading.dismiss();
        });
      /*
      }
      else {
        console.log('CHKOUT: ERROR -> Email client unavailable.');
        loading.dismiss();
      }
    })
    .catch((e) => {
      console.log('CHKOUT: A-ERROR -> ' + e);
      console.log('CHKOUT: ERROR -> Error in accessing Email Composer.');
      loading.dismiss();
    });
    */

  }

  setItems() {

    return new Promise((resolve, reject) => {
      this.orderListWhole.length = 0;

      this.platform.ready().then(() => {
        this.sqlite.create({
          name: Global.db,
          location: 'default'
        })
        .then((db: SQLiteObject) => {
          let sql = 'SELECT * FROM orderTable ' +
            'WHERE submitted = 0 ' +
            'AND user_id = ?';
          db.executeSql(sql, [Global.userid])
          .then((result) => {
            for(let i = 0; i < result.rows.length; i++) {
              new Promise((resolve) => {
                let orderElement = [
                  result.rows.item(i).shiptoid,
                  result.rows.item(i).contactid,
                  result.rows.item(i).ponumber,
                  result.rows.item(i).tagid,
                  result.rows.item(i).quantity,
                  result.rows.item(i).extdesc,
                  result.rows.item(i).timestamp
                ];
                resolve(orderElement)
              })
              .then((orderElement) => {
                this.orderListWhole.push([orderElement[0], orderElement[1], orderElement[2], orderElement[3], orderElement[4], orderElement[5], orderElement[6]]);
                this.orderList.push([orderElement[0], orderElement[1], orderElement[2], orderElement[3], orderElement[4], orderElement[5], orderElement[6]]);
              })
              .then(() => {
                if(i == (result.rows.length - 1)) {
                  resolve();
                }
              })
              .catch((e) => {
                reject(e);
              });
            }
          })
          .catch((e) => {
            reject(e);
          });
        })
        .catch((e) => {
          reject(e);
        });
      })
      .catch((e) => {
        reject(e);
      });

    });

  }

  resetItems() {

    this.orderList.length = 0;
    for(var i = 0; i < this.orderListWhole.length; i++) {
      this.orderList.push(this.orderListWhole[i]);
    }

  }

  deleteItem(target: any)
  {

    // SQLite
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      let sql = 'DELETE FROM orderTable ' +
        'WHERE timestamp = ? AND submitted = 0 AND user_id = ?';
      
      db.executeSql(sql, [target, Global.userid])
      .then(() => {
        console.log('Entry deleted');
        this.getCartSize()
        .then(() => {
          this.events.publish('menu:refresh');
          this.navCtrl.setRoot(CartPage);
        })
        .catch((e) => {
          console.log(e);
        })
      })
      .catch((e) => {
        console.log(e);
      });
    })
    .catch((e) => {
      console.log(e);
    });

  }

  confirmDelete(target: any)
  {

    let alertConfirm = this.alertCtrl.create({
      title: 'Delete Item',
      message: 'Do you want to delete chosen item?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteItem(target);
          }
        }
      ]
    });
    alertConfirm.present();

  }

  filterCartItems(ev: any) {

    this.resetItems();
    let val = ev.target.value;
    if(val && val.trim() !== '') {
      this.orderList = this.orderList.filter((item) => {
        let isMatching: boolean = false;
        for(let i = 0; i < item.length; i++) {
          if(item[i].toString().toLowerCase().indexOf(val.toLowerCase()) > (-1)) {
            isMatching = true;
            break;
          }
        }
        return isMatching;
      });
    }

  }

  pageRedirect(target: any, message: string, alerttype: string, showtime: number, waittime: number, loading: any) {

    if(target == 'order') {
      if(alerttype == 'toast') {
        let toast = this.toastCtrl.create({
          message: message,
          duration: showtime,
          position: 'bottom'
        });
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Order Page');
          this.navCtrl.setRoot(OrderPage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'toastok') {
        let toast = this.toastCtrl.create({
          message: message,
          showCloseButton: true,
          closeButtonText: 'OK',
          position: 'bottom'
        });
        
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Order Page');
          this.navCtrl.setRoot(OrderPage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'none') {
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Order Page');
          this.navCtrl.setRoot(OrderPage);
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
    }
    else if(target == 'cart') {
      if(alerttype == 'toast') {
        let toast = this.toastCtrl.create({
          message: message,
          duration: showtime,
          position: 'bottom'
        });
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Cart Page');
          this.navCtrl.setRoot(CartPage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'toastok') {
        let toast = this.toastCtrl.create({
          message: message,
          showCloseButton: true,
          closeButtonText: 'OK',
          position: 'bottom'
        });
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Cart Page');
          this.navCtrl.setRoot(CartPage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'none') {
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Cart Page');
          this.navCtrl.setRoot(CartPage);
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
    }
    else if(target == 'home') {
      if(alerttype == 'toast') {
        let toast = this.toastCtrl.create({
          message: message,
          duration: showtime,
          position: 'bottom'
        });
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Home Page');
          this.navCtrl.setRoot(HomePage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'toastok') {
        let toast = this.toastCtrl.create({
          message: message,
          showCloseButton: true,
          closeButtonText: 'OK',
          position: 'bottom'
        });
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Home Page');
          this.navCtrl.setRoot(HomePage);
          toast.present();
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
      else if(alerttype == 'none') {
        setTimeout(() => {
          this.events.publish('menu:refresh');
          console.log('REDIRECT: Redirecting to Home Page');
          this.navCtrl.setRoot(HomePage);
          setTimeout(() => {
            loading.dismiss();
          }, 100);
        }, waittime);
      }
    }
    
  }

}