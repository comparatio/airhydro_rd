import { Component } from '@angular/core';
import { NavController, LoadingController, MenuController } from 'ionic-angular';
import { DataProvider } from '../../providers/dataprovider';
import { Platform } from 'ionic-angular';

import { OrderPage } from '../order/order';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Global } from '../../app/global';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  orders = Array<Array<String>>();

  public deviceWidth = this.platform.width();

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private sqlite: SQLite,
    public menuCtrl: MenuController,
    public platform: Platform,
    public dataProv: DataProvider) {
    
    menuCtrl.swipeEnable(true);
    this.setData();

  }

  setData() {

    
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM orderList ' +
        'WHERE user_id = ? ' +
        'ORDER BY order_id DESC';
      db.executeSql(sql, [Global.userid])
      .then((result) => {
        for(let i = 0; i < result.rows.length; i++) {
          let orderitem = [
            result.rows.item(i).order_id,
            result.rows.item(i).timestamp,
            result.rows.item(i).size];
          this.orders.push(orderitem);
        }
      })
      .catch((e) => {
        console.log(e);
      });

    })
    .catch((e) => {
      console.log(e);
    });

  }

  refreshOrderList(refresher) {

    this.dataProv.getAsyncData().then((newData) => {
      /*
      for (var j = 0; j < 100; j++) {
        this.orders.pop();
      }
      for (var i = 0; i < 10; i++) {
        this.orders.push(newData[i]);
      }
      refresher.complete();
      */
    });

  }

  refreshPage() {
    
    let loading = this.loadingCtrl.create({
      content: 'Refreshing content...'
    });
    loading.present();
    setTimeout(() => {
      this.navCtrl.setRoot(HomePage);
      loading.dismiss();
    }, 1000);

  }

  pageRedirect(target: any) {

    if(target == 'order')
    {
      this.navCtrl.setRoot(OrderPage);
    }

  }

}
