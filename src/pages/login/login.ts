import { Component } from '@angular/core';
import { NavController, Platform, LoadingController, ViewController, ModalController, ToastController, Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { HTTP } from '@ionic-native/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';

import { HomePage } from '../home/home';

import { Global } from '../../app/global';
import { ConnectionVariables } from '../../app/connectionVariables';
import { Encryption } from '../../providers/cryptprovider';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  username: string = '';
  password: string = '';
  rememberme: boolean = false;
  failedlogin: boolean = false;

  public deviceheight = this.platform.height();
  public devicewidth = this.platform.width();

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public network: Network,
    private sqlite: SQLite,
    public events: Events,
    private storage: Storage,
    public crypt: Encryption,
    private http: HTTP,
    public loadingCtrl: LoadingController) {

      this.fetchLastValues();
      
      this.storage.get('userid').then((data) => {
        if(data != null) {
          Global.userid = data;
          this.checkUserStatus();
        }
      }).catch((e) => {
        console.log('Not present');
      });

  }

  checkUserStatus() {

    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    });

    this.sqlite.create({
      name: Global.db,
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      
      let sql = 'SELECT status, lastlogin FROM userProfiles ' +
        'WHERE user_id = ?';
      db.executeSql(sql, [Global.userid])
      .then((result) => {

        let userStatus = result.rows.item(0).status;
        if(userStatus == 1) {

          // Check difference between lastlogin and current timestamp
          let previous: any = new Date(result.rows.item(0).lastlogin);
          let current: any  = new Date();
          let currentval: number = current.valueOf();
          let prevval: number = previous.valueOf();
          let difference = currentval - prevval;

          let differencethreshold = Global.accesslimit*(60*1000); // Total value in ms
          if(difference <= differencethreshold) {

            // Redirect to home, since already logged in
            loading.present();
            this.events.publish('menu:refresh');
            this.loadLastValues();
            setTimeout(() => {
              this.navCtrl.setRoot(HomePage);
              loading.dismiss();
            }, 700);
            
          }
          else {
            console.log('Local storage entry found, but time out.');
          }

        }

      })
      .catch((e) => {
        console.log(e);
        alert('Issue in reading from userProfiles to check status');
      });

    })
    .catch((e) => {
      console.log(e);
    });

  }

  createErrorMessage() {
    // Open modal to create and send error message
    let modal = this.modalCtrl.create(ErrorMessageModal);
    modal.present();
  }

  fetchLastValues() {

    this.storage.get('username')
    .then((value) => {
      if(value != null) {
        this.username = value;
        this.storage.get('password')
        .then((val) => {
          if(val != null) {
            this.crypt.decryptDataAsPromise(val)
            .then((decryptedval) => {
              this.password = decryptedval.toString();
              this.rememberme = true;
            })
            .catch((e) => {
              console.log('Error in decryption function call.');
              console.log(e);
            });
          }
        })
        .catch((e) => {
          console.log('Error when getting previous password.');
        });
      }
      else {
        this.username = '';
        this.password = '';
        this.rememberme = false;
      }
    })
    .catch((e) => {
      console.log('Error when getting previous username.');
    });

  }

  loadLastValues() {

    this.storage.get('username')
    .then((val) => {
      Global.username = val.toString();
      this.storage.get('email')
      .then((email) => {
        Global.email = email;
      })
      .catch((e) => {
        console.log(e);
      });
    })
    .catch((e) => {
      console.log(e);
    });

  }

  logInSubmit() {

    if(this.username == '' || this.username == null || this.username == undefined
      || this.password == '' || this.password == null || this.password == undefined) {
      this.password = '';
      this.rememberme = false;
      alert('Please enter all fields');
      return;
    }

    let loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });
    
    let isOnline = this.network.type;

    if(isOnline != 'none') {
      loading.present();

      this.crypt.encryptDataAsPromise(this.username)
      .then((encryptres1) => {
        console.log('Username encryption: ' + encryptres1.toString());
        this.crypt.encryptDataAsPromise(this.password)
        .then((encryptres2) => {
          console.log('Password encryption: ' + encryptres2.toString());
          //let link = 'http://127.0.0.1/airhydrodemo.php';
          let data = {
            'action': 'login',
            'username': encryptres1.toString(),
            'password': encryptres2.toString()
          };
          this.http.setRequestTimeout(ConnectionVariables.timeout);
          this.http.acceptAllCerts(true)
          .then(() => {
            this.http.post(ConnectionVariables.httpUrl, data, {})
            .then((result) => {

              //let resp_parse = JSON.parse(result.data.toString());
              console.log('Response from server: ' + result.data);
              this.crypt.decryptDataAsPromise(result.data)
              .then((decryptval1) => {
                let res = JSON.parse(decryptval1.toString());

                if(res.status == 1) {

                  // User authenticated
                  this.crypt.decryptDataAsPromise(res.userid)
                  .then((decryptval2) => {
                    Global.userid = parseInt(decryptval2.toString());
                    this.crypt.decryptDataAsPromise(res.email)
                    .then((decryptval3) => {
                      Global.email = decryptval3.toString();
                      Global.username = this.username;
                      this.sqlite.create({
                        name: Global.db,
                        location: 'default'
                      })
                      .then((db: SQLiteObject) => {
        
                        let timestamp = new Date();
                        let sql = 'INSERT INTO userProfiles VALUES (?, ?, ?, ?, ?)'; // insert userid, username, email, status (set to 1), lastlogin
                        db.executeSql(sql, [Global.userid, this.username, Global.email, 1, timestamp])
                        .then(() => {
                          this.storage.set('userid', Global.userid);
                          this.setLastValues(Global.email);
                          if(this.rememberme) {
                            this.rememberMe();
                          }
                          console.log('Value inserted succesfully into userProfiles.');
        
                          setTimeout(() => {
                            this.navCtrl.setRoot(HomePage);
                            loading.dismiss();
                          }, 1200);
                        })
                        .catch((e) => {
                          console.log(e);
                          console.log('Error in inserting value into userProfiles table.');
        
                          sql = 'UPDATE userProfiles ' +
                            'SET status = ?, lastlogin = ? ' +
                            'WHERE user_id = ?';
                          db.executeSql(sql, [1, timestamp, Global.userid])
                          .then(() => {
                            this.storage.set('userid', Global.userid);
                            this.resetLastValues();
                            this.setLastValues(Global.email);
                            if(this.rememberme) {
                              this.rememberMe();
                            }
                            console.log('Updated existing entry.');
                            setTimeout(() => {
                              this.failedlogin = false;
                              this.navCtrl.setRoot(HomePage);
                              loading.dismiss();
                            }, 1200);
                          })
                          .catch((e) => {
                            console.log('Unable to update existing entry.');
                            this.failedlogin = true;
                            loading.dismiss();
                          });
                        });
        
                      })
                      .catch((e) => {
                        console.log('Error in connecting to db.');
                        this.failedlogin = true;
                        loading.dismiss();
                      });
                    })
                    .catch((e) => {
                      console.log(e);
                      this.failedlogin = true;
                      loading.dismiss();
                    });
                  })
                  .catch((e) => {
                    console.log(e);
                    this.failedlogin = true;
                    loading.dismiss();
                  });

                }
                else {

                  // User authentication failed
                  console.log('Error ' + res.status + ': ' + res.error);
                  this.failedlogin = true;
                  setTimeout(() => {
                    this.password = '';
                    this.failedlogin = true;
                    this.rememberme = false;
                    alert('Invalid credentials. Please try again.');
                    loading.dismiss();
                  }, 1000);
                  return;

                }

              })
              .catch((e) => {
                console.log(e);
                this.failedlogin = true;
                loading.dismiss();
              });
              
            })
            .catch((e) => {
              let toast = this.toastCtrl.create({
                message: 'Issue in connecting to server.',
                duration: 2500,
                position: 'bottom'
              });
              console.log(e);
              console.log('Issue in backend connection part.');
              this.failedlogin = true;
              setTimeout(() => {
                loading.dismiss();
                toast.present();
              }, 200);
            });
          })
          .catch((e) => {
            let toast = this.toastCtrl.create({
              message: 'Issue in connecting to server.',
              duration: 2500,
              position: 'bottom'
            });
            console.log(e);
            console.log('Issue in backend connection part.');
            this.failedlogin = true;
            setTimeout(() => {
              loading.dismiss();
              toast.present();
            }, 200);
          });
          
        })
        .catch((e) => {
          console.log(e);
          this.failedlogin = true;
          loading.dismiss();
        });
      })
      .catch((e) => {
        console.log(e);
        this.failedlogin = true;
        loading.dismiss();
      });

    }
    else {
      let toast = this.toastCtrl.create({
        message: 'No internet connection available. Try again later.',
        showCloseButton: true,
        closeButtonText: 'OK',
        position: 'bottom'
      });
      setTimeout(() => {
        toast.present();
      }, 300);
      return;
    }

  }

  resetLastValues() {

    this.storage.remove('username')
    .catch((e) => {
      console.log('Issue in removing last username.');
    });

    this.storage.remove('password')
    .catch((e) => {
      console.log('Issue in removing last password');
    });

    this.storage.remove('email')
    .catch((e) => {
      console.log('Issue in removing last email');
    });

    this.storage.remove('rememberme')
    .catch((e) => {
      console.log('Issue in removing rememberme');
    });
    
  }

  rememberMe() {
    this.crypt.encryptDataAsPromise(this.password)
    .then((encryptedval) => {
      this.storage.set('password', encryptedval)
      .then(() => {
        console.log('Password saved to storage.');
        this.storage.set('rememberme', true)
        .then(() => {
          console.log('Set rememberme value.');
        })
        .catch((e) => {
          console.log('Error in saving rememberme.');
        });
      })
      .catch((e) => {
        console.log('Error in saving password.');
      });
    })
    .catch((e) => {
      console.log('Error in encrypting password.');
    });
  }

  setLastValues(email: string) {

    this.resetLastValues();
    this.storage.set('username', this.username)
    .then(() => {
      console.log('Username saved in local storage.');
      this.storage.set('email', email)
      .then(() => {
        console.log('Email saved in local storage.');
      })
      .catch((e) => {
        console.log(e);
      });
    })
    .catch((e) => {
      console.log(e);
    });

  }

}

@Component({
  selector: 'page-login',
  templateUrl: 'errormessagemodal.html'
})
export class ErrorMessageModal {

  modalusername: string;
  description: string;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public http: HTTP,
    public crypt: Encryption,
    public viewCtrl: ViewController) {

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  sendErrorMessage() {

    let loading = this.loadingCtrl.create({
      content: 'Reporting issue...'
    });
    loading.present();

    // Send log report to server

    // Send username, message to server
    //let link = 'http://127.0.0.1/airhydrodemo.php';
    let data = {
      'action': 'reporterror',
      'username': this.modalusername,
      'message': this.description
    };

    console.log('REPERR: STATUS Starting exec -> Sending data to server.');
    this.http.setRequestTimeout(ConnectionVariables.timeout);
    this.http.acceptAllCerts(true)
    .then(() => {
      this.http.post(ConnectionVariables.httpUrl, data, {})
      .then((response) => {
        console.log('REPERR: Response from server => ' + response.data);
        this.crypt.decryptDataAsPromise(response.data)
        .then((decryptedresponse) => {
          let res = JSON.parse(decryptedresponse.toString());
          if(res.status == 1) {
            // Request successful

            console.log('REPERR: STATUS Ending exec -> Issue reported successfully.');
            let toast = this.toastCtrl.create({
              message: 'Issue reported. We will look into it!',
              duration: 2000,
              position: 'bottom'
            });
            setTimeout(() => {
              this.viewCtrl.dismiss();
              loading.dismiss();
              toast.present();
            }, 1000);

          }
          else if(res.status == 500) {
            // Internal server error

            console.log('REPERR: STATUS Ending exec -> Issue not reported.');
            console.log('REPERR: ERROR -> Error ' + res.status + ': ' + res.error);
            let toast = this.toastCtrl.create({
              message: 'Some issue at server. Please try again later.',
              duration: 2000,
              position: 'bottom'
            });
            setTimeout(() => {
              loading.dismiss();
              toast.present();
            }, 1000);

          }
          else {
            // Insufficient parameters provided

            console.log('REPERR: STATUS Ending exec -> Issue not reported.');
            console.log('REPERR: ERROR -> Error ' + res.status + ': ' + res.error);
            let toast = this.toastCtrl.create({
              message: 'Unexpected error encountered. Please try again later.',
              duration: 2000,
              position: 'bottom'
            });
            setTimeout(() => {
              this.viewCtrl.dismiss();
              loading.dismiss();
              toast.present();
            }, 1000);

          }
        })
        .catch((e) => {
          console.log('REPERR: ERROR -> Error in decrypting info.');
          console.log('REPERR: A-ERROR -> ' + e);
          let toast = this.toastCtrl.create({
            message: 'Unexpected error encountered. Please try again later.',
            duration: 2000,
            position: 'bottom'
          });
          setTimeout(() => {
            this.viewCtrl.dismiss();
            loading.dismiss();
            toast.present();
          }, 1000);
        });
      })
      .catch((e) => {
        console.log('REPERR: ERROR -> Error in sending data to server.');
        console.log('REPERR: A-ERROR -> ' + e);
        let toast = this.toastCtrl.create({
          message: 'Unexpected error encountered. Please try again later.',
          duration: 2000,
          position: 'bottom'
        });
        setTimeout(() => {
          this.viewCtrl.dismiss();
          loading.dismiss();
          toast.present();
        }, 1000);
      });
    })
    .catch((e) => {
      console.log('REPERR: ERROR -> Error in sending data to server.');
      console.log('REPERR: A-ERROR -> ' + e);
      let toast = this.toastCtrl.create({
        message: 'Unexpected error encountered. Please try again later.',
        duration: 2000,
        position: 'bottom'
      });
      setTimeout(() => {
        this.viewCtrl.dismiss();
        loading.dismiss();
        toast.present();
      }, 1000);
    });
    
  }

}