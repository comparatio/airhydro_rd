import { Component } from '@angular/core';
import { NavController, Platform, AlertController, App, LoadingController, ToastController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';

import { Global } from '../../app/global';
import { ConnectionVariables } from '../../app/connectionVariables';
import { LoginPage } from '../login/login';
import { Encryption } from '../../providers/cryptprovider';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  public devicewidth = this.platform.width();
  username: any;
  email: any;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public app: App,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    private storage: Storage,
    public http: HTTP,
    public crypt: Encryption,
    public alertCtrl: AlertController) {

      this.loadLastValues();

  }

  loadLastValues() {
    this.username = Global.username;
    this.email = Global.email;
  }  

  logout() {

    let alertConfirm = this.alertCtrl.create({
      title: 'Log Out',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            // Reset any global variables
            
            let loading = this.loadingCtrl.create({
              content: 'Logging out...'
            });
            let toast = this.toastCtrl.create({
              message: 'You have succesfully logged out!',
              duration: 1800,
              position: 'bottom'
            });

            //let link = 'http://127.0.0.1/airhydrodemo.php';
            let data = {
              'action': 'logout',
              'username': Global.username
            };

            this.http.setRequestTimeout(ConnectionVariables.timeout);
            this.http.acceptAllCerts(true)
            .then(() => {
              this.http.post(ConnectionVariables.httpUrl, data, {})
              .then((result) => {

                let res = JSON.parse(result.data);
                if(res.status == 1) {
                  // Logout successful

                  console.log('Logout request executed successfully at server.' + res.response);
                  this.sqlite.create({
                    name: Global.db,
                    location: 'default'
                  })
                  .then((db: SQLiteObject) => {
      
                    let sql = 'UPDATE userProfiles ' +
                      'SET status = 0 ' +
                      'WHERE user_id = ?';
                    db.executeSql(sql, [Global.userid])
                    .then(() => {
                      Global.userid = 0;
                      Global.email = '';
                      Global.username = '';
                      this.storage.remove('userid')
                      .catch((e) => {
                        console.log('No variable userid to remove from storage.');
                      });
                      this.storage.remove('email')
                      .catch((e) => {
                        console.log('No variable email to remove from storage.');
                      });
                      this.storage.get('rememberme')
                      .then((rememberme) => {
                        if(!rememberme) {
                          console.log('Rememberme not set.');
                          this.storage.remove('username')
                          .catch((e) => {
                            console.log('No variable username to remove from storage.');
                          });
                          
                          this.storage.remove('password')
                          .catch((e) => {
                            console.log('No variable password to remove from storage.');
                          });

                          this.storage.remove('rememberme')
                          .catch((e) => {
                            console.log('No variable rememberme to remove from storage.');
                          });
                        }
                      })
                      .catch((e) => {
                        console.log('Rememberme not found. Removing username & password.');
                        this.storage.remove('username')
                        .catch((e) => {
                          console.log('No variable username to remove from storage.');
                        });
                        
                        this.storage.remove('password')
                        .catch((e) => {
                          console.log('No variable password to remove from storage.');
                        });
                      });
                      
                      console.log('UserProfiles updated.');
                      loading.present();
                      setTimeout(() => {
                        this.app.getRootNav().setRoot(LoginPage);
                        loading.dismiss();
                        toast.present();
                      }, 1000);
                    })
                    .catch((e) => {
                      console.log(e);
                      console.log('UserProfile status update failed.');
                    });
                  })
                  .catch((e) => {
                    console.log(e);
                  });

                }
                else {
                  // Logout unsuccessful
                  console.log('Unable to log out.');
                }

              })
              .catch((e) => {
                console.log('Error in sending request to backend.');
                console.log(e);
              });
            })
            .catch((e) => {
              console.log('Error in connecting to backend.');
              console.log(e);
            });
            
          }
        }
      ]
    });
    alertConfirm.present();

  }

}
