import { Component, Injectable, Input, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, ViewController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import 'rxjs/add/operator/map';

import { CartPage } from '../cart/cart';
import { Global } from '../../app/global';

@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})

@Injectable()
export class OrderPage {

  shiptoid: any = '';
  contactid: any = '';
  ponumber: any = '';
  tagid: any = '';
  quantity: any = '';
  extdesc: any = '';

  @ViewChild('shiptoidid') ShiptoID;
  @ViewChild('contactidid') ContactID;
  @ViewChild('ponumberid') PONumber;
  @ViewChild('tagidid') TagID;
  @ViewChild('quantity') Quantity;
  @ViewChild('extdescid') ExtDesc;

  scanresult: any;

  options: BarcodeScannerOptions;

  public devicewidth = this.platform.width();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public barcodeScanner: BarcodeScanner,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public events: Events,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private sqlite: SQLite,
    public alertCtrl: AlertController,
    public platform: Platform) {

      this.fillLastValues();

  }

  addToCart() {

    // Check if all necessary fields are complete
    // Not being checked through 'required'
    if(this.shiptoid == null || this.shiptoid == '' || this.shiptoid == undefined 
      || this.tagid == null || this.tagid == '' || this.tagid == undefined 
      || this.quantity == null || this.quantity == '' || this.quantity == undefined) {
      // Return focus back to correct input item in form - not working
      alert('Please enter all required values correctly.');
      this.openModal();
      return;
    }

    else if(this.shiptoid.toString().length > 11) {
      alert('Ship to ID must be less than 10 digits.');
      this.openModal();
      return;
    }

    else if(this.contactid.toString().length > 10) {
      alert('Contact ID must be less than 10 digits.');
      this.openModal();
      return;
    }

    //Store values in local storage
    let valuestosave = {};
    valuestosave = [this.shiptoid, this.contactid, this.ponumber];
    this.saveLastValues(valuestosave);

    // Set to storage (timstamp, values-array)
    let timestamp: any;
    timestamp = new Date();
    timestamp = timestamp.toString();

    // SQLite
    this.platform.ready()
    .then(() => {

      this.sqlite.create({
        name: Global.db,
        location: 'default'
      })
      .then((db: SQLiteObject) => {

          // Check if existing entry is present

          let sql = 'SELECT * FROM orderTable ' +
            'WHERE shiptoid = ? ' +
            'AND contactid = ? ' +
            'AND ponumber = ? ' +
            'AND tagid = ?';

          db.executeSql(sql, [this.shiptoid, this.contactid, this.ponumber, this.tagid])
          .then((result) => {
            if(result.rows.length > 0) {
              alert('You have already scanned this item for same Ship to ID, Contact ID and PO Number.');
              return;
            }
            else {

              // Insert new item into cart
              sql = 'INSERT INTO orderTable(timestamp, shiptoid, contactid, ponumber, tagid, quantity, extdesc, submitted, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';

              db.executeSql(sql, [timestamp, this.shiptoid, this.contactid, this.ponumber, this.tagid, this.quantity, this.extdesc, 0, Global.userid])
              .then(() => {

                console.log('Entry inserted into table');
                this.events.publish('menu:refresh');

                // Old approach - locally creating a controller
                // Improved approach - have a provider and create its instance to call loader - not working
                let loading = this.loadingCtrl.create({
                  content: 'Processing...'
                });
                loading.present();
                
                setTimeout(() => {
                  this.navCtrl.setRoot(OrderPage);
                  let toast = this.toastCtrl.create({
                    message: 'Item added to cart',
                    duration: 1000,
                    position: 'middle'
                  });
                  loading.dismiss();
                  toast.present();
                }, 200);

              })
              .catch((e) => {
                console.log(e);
              });
              
            }
          })
          .catch((e) => {
            console.log(e);
          });

      })
      .catch((e) => {
        console.log(e);
      });

    })
    .catch((e) => {
      console.log(e);
    });

  }

  fillLastValues() {

    this.storage.get('last')
    .then((element) => {
      this.shiptoid = element[0];
      this.contactid = element[1];
      this.ponumber = element[2];
      setTimeout(() => {
        this.TagID.setFocus();
        console.log('INIT: Previous values loaded.');
      }, 1000);
    })
    .catch((e) => {
      console.log('INIT: No last values found.');
      setTimeout(() => {
        this.ShiptoID.setFocus();
      }, 200);
    });

  }

  openModal() {

    let modal = this.modalCtrl.create(OrderInfoModal);
    modal.present();

  }

  pageRedirect(target: any) {

    if(target == 'cart') {
      this.navCtrl.setRoot(CartPage);
    }

  }

  saveLastValues(valueset: {}) {

    console.log('ADDCRT: Saving form values to remember as \'last\'');
    this.storage.remove('last');
    let key: string = 'last';
    this.storage.set(key, valueset);

  }

  scan() {

    this.options = {
      showFlipCameraButton: false,
      preferFrontCamera: false,
      prompt: 'Scan a barcode',
      resultDisplayDuration: 200
    };

    return new Promise(resolve => {
      this.platform.ready()
      .then(() => {
        this.barcodeScanner.scan(this.options).then((result) => {
          if(!result.cancelled) {
            resolve(result.text);
          }
        }, (error) => {
          resolve('');
        });
      });
    });

  }

  scanCode(target: any) {

    this.scan()
    .then(result => {
      console.log('SCNFLD: Scan success.');
      if(target == 'shiptoid') {

        // If not a number, do not set value and provide toast
        if(isNaN(Number(result))) {
          // Generate toast
          let toast = this.toastCtrl.create({
            message: 'Ship to ID needs to be a number only.',
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
          });
          toast.present();
        }
        else {
          this.shiptoid = result;
          this.shiptoid = this.shiptoid.trim();
        }

      }
      else if(target == 'contactid') {

        // If not a number, do not set value and provide toast
        if(isNaN(Number(result))) {
          // Generate toast
          let toast = this.toastCtrl.create({
            message: 'Contact ID needs to be a number only.',
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
          });
          toast.present();
        }
        else {
          this.contactid = result;
          this.contactid = this.contactid.trim();
        }

      }
      else if(target == 'ponumber') {
        this.ponumber = result;
        this.ponumber = this.ponumber.trim();
      }
      else if(target == 'tagid') {
        this.tagid = result;
        this.tagid = this.tagid.trim();
      }
      else if(target == 'quantity') {

        // If not a number, do not set value and provide toast
        if(isNaN(Number(result))) {
          // Generate toast
          let toast = this.toastCtrl.create({
            message: 'Quantity needs to be a number only.',
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
          });
          toast.present();
        }
        else {
          this.quantity = result;
          this.quantity = this.quantity.trim();
        }

      }
    })
    .catch((e) => {
      console.log('Scan error: ' + e);
    });

  }

}

@Component({
  templateUrl: 'orderinfomodal.html'
})
export class OrderInfoModal {

  constructor(
    public platform: Platform,
    public viewCtrl: ViewController) {

  }

  dismiss() {

    this.viewCtrl.dismiss();

  }

}