import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IonicStorageModule } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite';
import { HTTP } from '@ionic-native/http';
import { EmailComposer } from '@ionic-native/email-composer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { OrderPage } from '../pages/order/order';
import { LoginPage } from '../pages/login/login';
import { CartPage } from '../pages/cart/cart';
import { ProfilePage } from '../pages/profile/profile';
import { AboutPage } from '../pages/about/about';
import { OrderInfoModal } from '../pages/order/order';
import { ErrorMessageModal } from '../pages/login/login';

import { DataProvider } from '../providers/dataprovider'
import { Network } from '@ionic-native/network';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Encryption } from '../providers/cryptprovider';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    OrderPage,
    LoginPage,
    CartPage,
    ProfilePage,
    AboutPage,
    OrderInfoModal,
    ErrorMessageModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    OrderPage,
    LoginPage,
    CartPage,
    ProfilePage,
    AboutPage,
    OrderInfoModal,
    ErrorMessageModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    SQLite,
    Network,
    Storage,
    HTTP,
    Encryption,
    BarcodeScanner,
    EmailComposer
  ]
})
export class AppModule {}
