import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { HomePage } from '../pages/home/home';
import { OrderPage } from '../pages/order/order';
import { LoginPage } from '../pages/login/login';
import { CartPage } from '../pages/cart/cart';
import { ProfilePage } from '../pages/profile/profile';
import { AboutPage } from '../pages/about/about';
import { Storage } from '@ionic/storage';
import { Global } from './global';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  //itemsincart = Array<String>();
  cartSize: any;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public sqlite: SQLite,
    public events: Events) {

    this.initializeApp();
    this.refreshMenu();

    events.subscribe('menu:refresh', () => {
      this.refreshMenu();
    });

    // used for an example of ngFor and navigation
    // Controls the navigation menu
    this.pages = [
      { title: 'My Orders', icon: 'list', component: HomePage },
      { title: 'Build Order', icon: 'create', component: OrderPage },
      { title: 'My Cart', icon: 'cart', component: CartPage },
      { title: 'Profile', icon: 'contact', component: ProfilePage},
      { title: 'About', icon: 'information-circle', component: AboutPage}
    ];

  }

  refreshMenu() {

    // Get number of cart entries

    // SQLite read
    console.log('Reading from SQLite.');
    let dbname = 'data.db';
    this.sqlite.create({
      name: dbname,
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      // Fetch number of items in cart
      let sql = 'SELECT COUNT(timestamp) as itemsincart FROM orderTable ' +
        'WHERE submitted = 0 ' +
        'AND user_id = ?';
      db.executeSql(sql, [Global.userid])
      .then((result) => {
        this.cartSize = result.rows.item(0).itemsincart;
        //alert(this.cartSize);
        console.log('Updated cart size.');
      })
      .catch((e) => {
        console.log(e);
      });
    })
    .catch((e) => {
      console.log(e);
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.createTables();
      this.refreshMenu();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  createTables() {
    console.log('APPINI: Creating/opening database');
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
  
        console.log('APPINI: Creating tables, if not present');
  
        let sql = 'CREATE TABLE IF NOT EXISTS orderTable (' +
          'timestamp VARCHAR(128) PRIMARY KEY, ' +
          'shiptoid INTEGER, ' +
          'contactid INTEGER, ' +
          'ponumber VARCHAR(255), ' +
          'tagid VARCHAR(128), ' +
          'quantity INTEGER, ' +
          'extdesc VARCHAR(255), ' +
          'submitted TINYINT(2) DEFAULT 0, '+
          'user_id INTEGER)';
        db.executeSql(sql, {})
        .then(() => {
          console.log('APPINI: Status -> OrderTable table created.');
          //alert('OrderTable table created');
          sql = 'CREATE TABLE IF NOT EXISTS orderList (' +
            'order_id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
            'timestamp DATETIME, ' +
            'size INTEGER NOT NULL, ' +
            'user_id INTEGER)';
          db.executeSql(sql, {})
          .then(() => {
            console.log('APPINI: Status -> OrderList table created.');
            //alert('OrderList table created');
            sql = 'CREATE TABLE IF NOT EXISTS userProfiles (' +
              'user_id INTEGER PRIMARY KEY, ' +
              'username VARCHAR(128), ' +
              'email VARCHAR(128), ' +
              'status TINYINT(2) DEFAULT 0, ' +
              'lastlogin DATETIME)'; // 1 is logged in, 0 is logged out
            db.executeSql(sql, {})
            .then(() => {
              console.log('APPINI: Status -> UserProfiles table created.');
            })
            .catch((e) => {
              console.log('APPINI: A-ERROR -> ' + e);
              console.log('APPINI: Error -> UserProfiles table creation failed.');
            });
          })
          .catch((e) => {
            console.log('APPINI: A-ERROR -> ' + e);
            console.log('APPINI: Error -> OrderList table creation failed.');
            //alert('OrderList table creation failed.');
          });
        })
        .catch((e) => {
          console.log('APPINI: A-ERROR -> ' + e);
          console.log('APPINI: Error -> OrderTable table creation failed.');
        });
      })
      .catch((e) => {
        console.log('APPINI: A-ERROR -> ' + e);
        console.log('APPINI: Error -> Database access error.');
      });
    })
    .catch((e) => {
      console.log('APPINI: A-ERROR -> ' + e);
      console.log('APPINI: Error -> Platform not ready.');
    });
    
  }

}
