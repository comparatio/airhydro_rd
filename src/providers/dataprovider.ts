import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DataProvider {

  constructor(private sqlite: SQLite) {

  }

  getData(): any[] {
    // Return mock data asynchronously
    let data: any[] = [];
    
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {

      let sql = 'SELECT * FROM orderList';
      db.executeSql(sql, {})
      .then((result) => {
        for(let i = 0; i < result.rows.length; i++) {
          let orderinfo = [
            result.rows.item(i).order_id,
            result.rows.item(i).timestamp,
            result.rows.item(i).size
          ];
          data.push(orderinfo);
        }
      })

    })
    .catch((e) => {
      console.log(e);
    });

    return data;
  }

  getAsyncData(): Promise<any[]> {
    // Async receive mock data
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(this.getData());
      }, 1000);
    });
  }

}
