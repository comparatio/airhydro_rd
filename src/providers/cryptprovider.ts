import { Injectable } from '@angular/core';

@Injectable()
export class Encryption {

    hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];

    /*
    decryptData(data: string): string {

        let crypt_data = [
            -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
            -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
            -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
            -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
            -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
            -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
            104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
            -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
            43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
            -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
            -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
            39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
            76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
            -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
            -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
            -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
            33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
            -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
            40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
            -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
            103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
            -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
            -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
            2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
            5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
            97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
            -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
            104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
            -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
            -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
            -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
            0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
            -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
            3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
            23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
            14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
            -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
            -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
            46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
            60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
        ];

        if(data != null && data != '' && data != undefined) {
            
            // getDecCode()
            let ddata: string = '';
            for(let index = 0; index < (data.length - 1); index = index + 2) {
                ddata += String.fromCharCode(this.getDec(data.substring(index, index + 2)));
            }

            // getDecryptStageTwo()
            let uddata: string = '';
            for(let index = 0; index < ddata.length; index++) {
                let localvalue = ddata.charCodeAt(index) ^ crypt_data[(index % crypt_data.length)];
                while(localvalue < 0) {
                    localvalue += 256;
                }
                uddata += String.fromCharCode(localvalue);
            }

            data = uddata;
            // Uncompress data

        }

        return data;

    }

    encryptData(data: string): string {

        let crypt_data = [
            -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
            -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
            -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
            -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
            -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
            -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
            104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
            -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
            43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
            -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
            -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
            39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
            76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
            -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
            -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
            -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
            33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
            -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
            40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
            -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
            103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
            -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
            -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
            2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
            5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
            97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
            -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
            104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
            -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
            -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
            -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
            0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
            -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
            3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
            23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
            14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
            -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
            -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
            46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
            60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
        ];

        if(data != null && data != '' && data != undefined) {
            
            // Compress data

            // getEncryptStageOne()
            let cdata: string = '';
            for(let index = 0; index < data.length; index++) {
                let localComputed = data.charCodeAt(index) ^ crypt_data[(index % crypt_data.length)];
                while(localComputed < 0) {
                    localComputed += 256;
                }
                cdata += String.fromCharCode(localComputed);
            }

            // getHexCode()
            let hcdata: string = '';
            for(let index = 0; index < cdata.length; index++) {
                let hex = (this.getHex(cdata.charCodeAt(index))).toUpperCase();
                if(hex.length == 1) {
                    hex = '0' + hex;
                }
                hcdata += hex;
            }
            data = hcdata;
        }

        return data;

    }
    */

    encryptDataAsPromise(data: string) {

        return new Promise((resolve, reject) => {
            let crypt_data = [
                -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
                -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
                -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
                -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
                -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
                -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
                104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
                -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
                43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
                -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
                -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
                39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
                76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
                -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
                -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
                -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
                33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
                -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
                40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
                -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
                103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
                -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
                -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
                2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
                5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
                97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
                -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
                104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
                -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
                -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
                -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
                0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
                -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
                3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
                23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
                14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
                -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
                -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
                46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
                60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
            ];
            if(data != null && data != '' && data != undefined) {
                this.getEncryptStageOne(data, crypt_data)
                .then((cdata) => {
                    this.getHexCode(cdata.toString())
                    .then((hcdata) => {
                        resolve(hcdata.toString());
                    })
                    .catch((e) => {
                        reject(e);
                    });
                })
                .catch((e) => {
                    reject(e);
                });
            }
            else {
                reject('No data found.');
            }
        });

    }

    decryptDataAsPromise(data: string) {

        return new Promise((resolve, reject) => {
            let crypt_data = [
                -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
                -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
                -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
                -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
                -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
                -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
                104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
                -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
                43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
                -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
                -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
                39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
                76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
                -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
                -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
                -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
                33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
                -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
                40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
                -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
                103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
                -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
                -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
                2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
                5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
                97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
                -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
                104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
                -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
                -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
                -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
                0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
                -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
                3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
                23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
                14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
                -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
                -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
                46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
                60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
            ];
            if(data != null && data != '' && data != undefined) {
                console.log('Data found. Initiating process.');
                //alert('Data found. Initiating process.');
                this.getDecCode(data)
                .then((ddata) => {
                    console.log('Decimal obtained.');
                    //alert('Decimal obtained.');
                    this.getDecryptStageTwo(ddata.toString(), crypt_data)
                    .then((uddata) => {
                        console.log('Decryption complete. Returning promise.');
                        //alert('Decryption complete. Returning promise.');
                        resolve(uddata.toString());
                    })
                    .catch((e) => {
                        reject(e);
                    });
                })
                .catch((e) => {
                    //alert(e);
                    console.log(e);
                    reject(e);
                });
            }
            else {
                reject('No data found.');
            }
        });

    }

    /*getDecCode(data: string) {
        return new Promise((resolve, reject) => {
            let ddata: string = '';
            for(let i = 0; i < (data.length - 1); i += 2) {
                this.getDec(data.substring(i, i+2))
                .then((dec) => {
                    ddata += String.fromCharCode(Number(dec));
                    if((i+2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
            }
        });
    }*/

    getDecCode(data: string) {
        return new Promise((resolve, reject) => {
            let ddata: string = '';
            for(let i = 0; i < (data.length - 1); i += 2) {
                //ddata += String.fromCharCode(parseInt(data.substring(i, i+2), 16));
                /*this.getDec(data.substring(i, i+2))
                .then((dec) => {
                    ddata += String.fromCharCode(Number(dec));
                    if((i+2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                .catch((e) => {
                    reject(e);
                });*/
                let appendingstatement = new Promise((resolve, reject) => {
                    ddata += String.fromCharCode(parseInt(data.substring(i, i+2), 16));
                    setTimeout(() => {
                        resolve();
                    }, 10);
                })
                .then(() => {
                    if((i + 2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });
            }
        });
    }

    getDecryptStageTwo(ddata: string, crypt_data: any) {
        return new Promise((resolve, reject) => {
            let uddata: string = '';
            for(let i = 0; i < ddata.length; i++) {
                uddata += String.fromCharCode((((ddata.charCodeAt(i) ^ crypt_data[(i % crypt_data.length)])%256) + 256)%256);
                if(i == (ddata.length - 1)) {
                    resolve(uddata);
                }
            }
        });
    }

    getEncryptStageOne(data: string, crypt_data: any) {
        return new Promise((resolve, reject) => {
            let cdata: string = '';
            for(let i = 0; i < data.length; i++) {
                cdata += String.fromCharCode((((data.charCodeAt(i) ^ crypt_data[(i % crypt_data.length)])%256) + 256)%256);
                if(i == (data.length - 1)) {
                    resolve(cdata);
                }
            }
        });
    }
    
    getHexCode(cdata: string) {
        return new Promise((resolve, reject) => {
            let hcdata: string = '';
            for(let i = 0; i < cdata.length; i++) {
                //alert((i + 1) + ': ' + cdata.charCodeAt(i));
                /*
                this.getHex(cdata.charCodeAt(i))
                .then((hex) => {
                    //alert(hex.toString());
                    if(hex.toString().length == 1) {
                        let newhex = '0' + hex.toString();
                        hcdata += newhex.toUpperCase();
                        if(i == (cdata.length - 1)) {
                            resolve(hcdata);
                        }
                    }
                    else
                    {
                        hcdata += hex.toString().toUpperCase();
                        if(i == (cdata.length - 1)) {
                            resolve(hcdata);
                        }
                    }
                })
                .catch((e) => {
                    //alert('Error in getting hexCode.');
                    reject(e);
                });
                */
               
                /*
                let calculatehex = new Promise((resolve, reject) => {
                    let hex = cdata.charCodeAt(i).toString(16).toUpperCase();
                    setTimeout(() => {
                        resolve(hex);
                    }, 10);
                })
                .then((hex) => {
                    if(hex.toString().length == 1) {
                        //hex = '0' + hex;
                        hcdata += '0' + hex;
                    }
                })
                .then(() => {
                    if(i == (cdata.length - 1)) {
                        resolve(hcdata);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });
                */
                let hex = cdata.charCodeAt(i).toString(16).toUpperCase();
                if(hex.length == 1) {
                    hcdata += '0' + hex;
                }
                else {
                    hcdata += hex;
                }
                if(i == (cdata.length - 1)) {
                    resolve(hcdata);
                }
               
            }
        });
    }

    /*
    getDec(input: string): number {

        let decimal: number = 0;
        let hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        for(let index = (input.length - 1); index >= 0; index--) {
            decimal = decimal + (this.getPower(16, (input.length - 1 - index)) * hexArray.indexOf(input.charAt(index)));
        }
        return decimal;

    }
    */

    /*
    // Not needed
    getDec(input: string) {
        return new Promise((resolve, reject) => {
            let decimal: number = 0;
            for(let i = (input.length - 1); i >= 0; i--) {
                decimal += ((this.getPower(16, (input.length - 1 - i)) * this.hexArray.indexOf(input.charAt(i))));
                if(i==0) {
                    resolve(decimal);
                }
            }
        });
    }*/

    /*
    getHex(input: number): string {
        
        let hex: string = '';
        let hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        while(input > 0) {
            let remainder: number = input % 16;
            hex = hexArray[remainder] + hex;
            input = input / 16;
            input = parseInt(input.toString());
        }
        return hex;

    }
    */

    /*
    // Not needed
    getHex(input: number) {
        return new Promise((resolve, reject) => {
            let hex: string = '';*/
            /*
            let inputCopy = input;
            for(let i = input; i > 0; i = parseInt((inputCopy/16).toString())) {
                hex = this.hexArray[inputCopy%16].toString() + hex;
                //alert(this.hexArray[inputCopy%16].toString());
                inputCopy = parseInt((inputCopy/16).toString());
                //alert('New value of inputCopy ' + inputCopy);
                if((i-1) <= 0) {
                    alert('Returning hex' + hex);
                    resolve(hex);
                }
            }
            */
           
            /*
            while(input > 0) {
               hex = this.hexArray[input % 16] + hex;
               input = input/16;
               input = parseInt(input.toString());
               if(input == 0) {
                   //alert('Returning hex' + hex);
                   resolve(hex);
               }
           }
        });
    }
    */

    // Not needed
    /*
    getPower(base: number, exponent: number): number {

        let result = 1;
        if(exponent == 0) {
            return result;
        }
        else {
            for(var i = 0; i < exponent; i++) {
                result *= base;
                if(i == (exponent - 1)) {
                    return result;
                }
            }
        }

    }*/

    // Old technique, having issues
    /*
    compressData (data: string) {
        let p: string = '';
        let dictionary: Array<String> = [];
        let result: Array<{en: number}> = [];
        dictionary = this.fillASCIIValues();
        for(let i = 0; i < data.length; i++) {
            //alert(dictionary.length);
            if(i == (data.length - 1)) {
                if(dictionary.indexOf((p + data.charAt(i).toString())) != (-1)) {
                    p = p + data.charAt(i).toString();
                    if(p != '') {
                        result.push({en: dictionary.indexOf(p)});
                        //alert(result.length);
                        return this.getArrayAsString(result);
                    }
                    else {
                        //alert(result.length);
                        return this.getArrayAsString(result);
                    }
                }
                else {
                    result.push({en: dictionary.indexOf(p)});
                    dictionary.push((p + data.charAt(i).toString()));
                    p = data.charAt(i).toString();
                    if(p != '') {
                        result.push({en: dictionary.indexOf(p)});
                        //alert(result.length);
                        return this.getArrayAsString(result);
                    }
                    else {
                        //alert(result.length);
                        return this.getArrayAsString(result);
                    }
                }
            }
            else {
                if(dictionary.indexOf((p + data.charAt(i).toString())) != (-1)) {
                    //alert('GOT HERE FIRST');
                    //alert('Checked: ' + (p + data.charAt(i).toString()));
                    p = p + data.charAt(i).toString();
                }
                else {
                    //alert('Got elsewhere first');
                    //alert('Checked: ' + (p + data.charAt(i).toString()));
                    result.push({en: dictionary.indexOf(p)});
                    dictionary.push((p + data.charAt(i).toString()));
                    p = data.charAt(i).toString();
                }
            }
        }

    }

    decompressData(data: string) {
        let dataparts: Array<String> = data.split(",");
        let p: string = String.fromCharCode(Number(dataparts[0]));
        let result: string = p;
        let entry: string = "";
        let dictionary: Array<String> = [];
        dictionary = this.fillASCIIValues();
        for(let i = 1; i < dataparts.length; i++) {
          if(i == (dataparts.length - 1)) {
            if(dictionary.length > Number(dataparts[i])) {
              entry = dictionary[Number(dataparts[i])].toString();
              result += entry;
              dictionary.push(p + entry.charAt(0));
              p = entry;
              return result;
            }
            else {
              if(dictionary.length == Number(dataparts[i])) {
                entry = p + p.charAt(0);
                result += entry;
                dictionary.push(p + entry.charAt(0));
                p = entry;
                return result;
              }
              else {
                return '';
              }
            }
          }
          else {
            if(dictionary.length > Number(dataparts[i])) {
              entry = dictionary[Number(dataparts[i])].toString();
              result += entry;
              dictionary.push(p + entry.charAt(0));
              p = entry;
            }
            else {
              if(dictionary.length == Number(dataparts[i])) {
                entry = p + p.charAt(0);
                result += entry;
                dictionary.push(p + entry.charAt(0));
                p = entry;
              }
              else {
                return '';
              }
            }
          }
        }
      }
      */

    /*
    Returns an array containing ascii characters as array of String in appropriate position
    */
    fillASCIIValues() {

        let asciiset: Array<String> = [];
        for(let i = 0; i < 256; i++) {
            asciiset.push(String.fromCharCode(i));
            if(i == 255) {
                return asciiset;
            }
        }

    }

    /*
    Returns a string that is a compilation of the array, separated by commas
    */
    getArrayAsString(result: Array<Number>) {
        let res = '';
        for(let i = 0; i < result.length; i++) {
            if(i == (result.length - 1)) {
                res += result[i].toString();
                return res;
            }
            else {
                res += result[i].toString() + ',';
            }
        }
    }

    compress(uncompressed: string) {
        let dictionary: Array<String> = [];
        dictionary = this.fillASCIIValues();
        let result: Array<Number> = [];
        let w: string = "";
        for(let i = 0; i < uncompressed.length; i++) {
            if(i == (uncompressed.length-1)) {
                if(dictionary.indexOf(w + this.charAt(uncompressed, i)) != (-1)) {
                    w = w + this.charAt(uncompressed, i);
                    if(w != '') {
                        result.push(dictionary.indexOf(w));
                        return this.getArrayAsString(result);
                    }
                    else {
                        return this.getArrayAsString(result);
                    }
                }
                else {
                    result.push(dictionary.indexOf(w));
                    dictionary.push(w + this.charAt(uncompressed, i));
                    w = this.charAt(uncompressed, i);
                    if(w != '') {
                        result.push(dictionary.indexOf(w));
                        return this.getArrayAsString(result);
                    }
                    else {
                        return this.getArrayAsString(result);
                    }
                }
            }
            else {
                if(dictionary.indexOf(w + this.charAt(uncompressed, i)) != (-1)) {
                    w = w + this.charAt(uncompressed, i);
                }
                else {
                    result.push(dictionary.indexOf(w));
                    dictionary.push(w + this.charAt(uncompressed, i));
                    w = this.charAt(uncompressed, i);
                }
            }
        }
    }

    decompress(compressed: string) {
        let data: Array<String> = compressed.split(",");
        let dictionary: Array<String> = [];
        dictionary = this.fillASCIIValues();
        let w: string = String.fromCharCode(Number(data[0]));
        let result: string = w;
        for(let i = 1; i < data.length; i++) {
            if(i == (data.length - 1)) {
                let entry: string = '';
                let k: number = Number(data[i]);
                if(k < dictionary.length) {
                    entry = dictionary[k].toString();
                    result += entry;
                    dictionary.push(w + this.charAt(entry, 0));
                    w = entry;
                    return result;
                }
                else if(k == dictionary.length) {
                    entry = w + this.charAt(w, 0);
                    result += entry;
                    dictionary.push(w + this.charAt(entry, 0));
                    w = entry;
                    return result;
                }
            }
            else {
                let entry: string = '';
                let k: number = Number(data[i]);
                if(k < dictionary.length) {
                    entry = dictionary[k].toString();
                    result += entry;
                    dictionary.push(w + this.charAt(entry, 0));
                    w = entry;
                }
                else if(k == dictionary.length) {
                    entry = w + this.charAt(w, 0);
                    result += entry;
                    dictionary.push(w + this.charAt(entry, 0));
                    w = entry;
                }
            }
        }
    }

    charAt(data: string, position: number) {
        if(position < data.length) {
            return data.substr(position, 1);
        }
        else {
            return (-1).toString();
        }
    }

}