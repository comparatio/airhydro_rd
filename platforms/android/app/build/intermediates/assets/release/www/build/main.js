webpackJsonp([0],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_order__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_email_composer__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_global__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_connectionVariables__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_cryptprovider__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












let CartPage = CartPage_1 = class CartPage {
    constructor(navCtrl, navParams, storage, alertCtrl, sqlite, platform, network, toastCtrl, loadingCtrl, http, crypt, emailComposer, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.platform = platform;
        this.network = network;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.crypt = crypt;
        this.emailComposer = emailComposer;
        this.events = events;
        this.deviceWidth = this.platform.width();
        // Array that is copied from the Whole array, and is updated upon search within this page
        this.orderList = Array();
        // Actual array that reads from storage only once
        this.orderListWhole = Array();
        // Maintain temporary list of entries sent to server
        this.extractedItemTS = Array();
        this.setItems()
            .then(() => {
            // Update cart size to size set before previous Checkout initiation
            storage.get('cartsize')
                .then((val) => {
                this.cartsize = Number(val.toString());
                console.log('Local value for cartsize updated to ' + this.cartsize + '.');
                // Check if checkout process is in progress
                storage.get('checkoutInProgress')
                    .then((val) => {
                    if (val == 'true') {
                        console.log('Resuming checkout progress.');
                        this.checkoutProcess();
                    }
                    else {
                        console.log('Checkout not in progress.');
                    }
                })
                    .catch((e) => {
                    console.log('Error in reading checkoutInProgress from storage.');
                });
            })
                .catch((e) => {
                console.log('Error in fetching original cart size.');
            });
        })
            .catch((e) => {
            console.log('Unable to set items.');
            console.log(e);
        });
    }
    ngOnInit() {
    }
    addMore() {
        let loading = this.loadingCtrl.create({
            content: 'Loading...'
        });
        loading.present();
        this.pageRedirect('order', '', 'none', 0, 200, loading);
    }
    getTop20EntriesAsSQL() {
        console.log('CHKOUT: Call to getTop20EntriesAsSQL.');
        let backend_sql = '{';
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                let sql = 'SELECT * from orderTable ' +
                    'WHERE submitted = 0 ' +
                    'AND user_id = ? ' +
                    'ORDER BY ponumber, shiptoid, timestamp ' +
                    'LIMIT 20';
                console.log('CHKOUT: Initiate fetching rows from SQLite.');
                db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                    .then((result) => {
                    if (result.rows.length == 0) {
                        reject('CHKOUT: ERROR in getTop20EntriesAsSQL -> No rows found.');
                    }
                    else {
                        console.log('CHKOUT: Building string of paramset.');
                        for (let i = 0; i < result.rows.length; i++) {
                            this.extractedItemTS.push(result.rows.item(i).timestamp.toString());
                            // Build string containing parameters (equivalient to outcome of stringify on JSON object)
                            let paramset = '{' +
                                '"status": "0", ' +
                                '"ponumber": "' + result.rows.item(i).ponumber + '", ' +
                                '"scannedby": "' + __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid + '", ' +
                                '"qty": "' + result.rows.item(i).quantity + '", ' +
                                '"tagid": "' + result.rows.item(i).tagid + '", ' +
                                '"shiptoid": "' + result.rows.item(i).shiptoid + '", ' +
                                '"contactid": "' + result.rows.item(i).contactid + '", ' +
                                '"extdesc": "' + result.rows.item(i).extdesc + '", ' +
                                '"createdby": "' + __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid + '"' +
                                '}';
                            // Encrypt each record (since encryption of long strings is not working)
                            this.crypt.encryptDataAsPromise(paramset)
                                .then((param_enc) => {
                                let entry = '"' + (i + 1) + '": "' + param_enc.toString() + '"';
                                return entry;
                            })
                                .then((entry) => {
                                if (i != 0) {
                                    backend_sql += ', ' + entry;
                                }
                                else {
                                    backend_sql += entry;
                                }
                            })
                                .then(() => {
                                if (i == (result.rows.length - 1)) {
                                    console.log('CHKOUT: Completed building param string.');
                                    resolve(backend_sql + '}');
                                }
                            })
                                .catch((e) => {
                                console.log(e);
                            });
                        }
                    }
                })
                    .catch((e) => {
                    reject(e);
                });
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    updateOrderTable(cartSize) {
        console.log('CHKOUT: Call to updateOrderTable.');
        // Return cart size after decrement
        let count = 0;
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                for (let i = 0; i < this.extractedItemTS.length; i++) {
                    let sql = 'UPDATE orderTable ' +
                        'SET submitted = 1 ' +
                        'WHERE timestamp = ? ' +
                        'AND user_id = ?';
                    console.log('CHKOUT: Initiate update rows in SQLite.');
                    db.executeSql(sql, [this.extractedItemTS[i], __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                        .then(() => {
                        cartSize--;
                        count++;
                        console.log('CHKOUT: STATUS in updateOrderTable -> Decremented cart size by 1. Now contains ' + cartSize + ' items.');
                        if (i == (this.extractedItemTS.length - 1)) {
                            console.log('Updated ' + count + ' entries.');
                            resolve(cartSize);
                        }
                    })
                        .catch((e) => {
                        reject(e);
                    });
                }
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    updateOrderList(numberOfItems) {
        console.log('CHKOUT: Call to updateOrderList.');
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                console.log('CHKOUT: Inserting entry of ' + numberOfItems + ' items.');
                let timestamp = new Date();
                let sql = 'INSERT INTO orderList(timestamp, size, user_id) ' +
                    'VALUES (?, ?, ?)';
                console.log('CHKOUT: Initiate update rows in SQLite.');
                db.executeSql(sql, [timestamp, numberOfItems, __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                    .then(() => {
                    resolve('Success');
                })
                    .catch((e) => {
                    reject(e);
                });
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    checkoutProcess() {
        console.log('CHKOUT: In Checkout process.');
        // Proceed only if network is available
        let isOnline = this.network.type;
        // Check if network is available
        if (isOnline == 'none') {
            // No network connection
            console.log('CHKOUT: ERROR -> Internet unavailable to proceed.');
            let toast = this.toastCtrl.create({
                message: 'No internet connection available. Try again later.',
                showCloseButton: true,
                closeButtonText: 'OK',
                position: 'bottom'
            });
            toast.present();
            return;
        }
        else {
            // Network connection available
            let loading = this.loadingCtrl.create({
                content: 'Submitting order...'
            });
            loading.present();
            // Send to server
            /*
            sd1_status
            sd1_po_number     ponumber
            sd1_scanned_on    timestamp
            sd1_scanned_by    userid
            sd1_scanned_qty   quantity
            sd1_item_id       product
            ship_to_id        shiptoid
            created_by        ?userid
            created_on        ?timestamp
            
            // Send as batches
            */
            let cartSize = this.orderListWhole.length;
            //alert(cartSize);
            if (this.orderListWhole.length <= 0) {
                console.log('CHKOUT: ERROR -> No items to checkout.');
                this.storage.remove('attemptCount')
                    .then(() => {
                    this.storage.remove('checkoutInProgress')
                        .then(() => {
                        console.log('CHKOUT: STATUS Ending exec -> Removed attemptCount & checkoutInProgress entries from storage.');
                        this.pageRedirect('home', 'Completed checkout of all possible order items.', 'toast', 2000, 2000, loading);
                    })
                        .catch((e) => {
                        console.log('CHKOUT: A-ERROR -> ' + e);
                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                    });
                })
                    .catch((e) => {
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                });
            }
            else {
                let backend_sql = '';
                //console.log('Fetching top 20 entries and creating SQL.');
                this.getTop20EntriesAsSQL()
                    .then((result) => {
                    isOnline = this.network.type;
                    if (isOnline != 'none') {
                        // Network available to write to server
                        backend_sql += result.toString();
                        //let link = 'http://127.0.0.1/airhydrodemo.php';
                        this.crypt.encryptDataAsPromise(backend_sql)
                            .then((encryptedval) => {
                            let data = {
                                'action': 'insert',
                                'params': backend_sql
                            };
                            console.log('CHKOUT: Backend request content => ' + backend_sql);
                            console.log('CHKOUT: Posting fetched rows to server.');
                            this.http.setRequestTimeout(__WEBPACK_IMPORTED_MODULE_10__app_connectionVariables__["a" /* ConnectionVariables */].timeout);
                            this.http.acceptAllCerts(true)
                                .then(() => {
                                this.http.post(__WEBPACK_IMPORTED_MODULE_10__app_connectionVariables__["a" /* ConnectionVariables */].httpUrl, data, {})
                                    .then((response) => {
                                    console.log('CHKOUT: Server response => ' + response.data);
                                    this.crypt.decryptDataAsPromise(response.data)
                                        .then((decryptedval) => {
                                        console.log('CHKOUT: Decrypted => ' + decryptedval.toString());
                                        let res = JSON.parse(decryptedval.toString());
                                        if (res.status == 1) {
                                            console.log('CHKOUT: Positive response from server.');
                                            this.updateOrderTable(cartSize)
                                                .then((newcartsize) => {
                                                let itemsCheckedOut = this.cartsize - Number(newcartsize);
                                                cartSize = Number(newcartsize);
                                                if (cartSize == 0) {
                                                    // All items checked out
                                                    console.log('CHKOUT: All items checked out. Inserting into orderList.');
                                                    this.updateOrderList(itemsCheckedOut)
                                                        .then((result) => {
                                                        // Remove 'checkoutInProgress' key-value pair from storage
                                                        this.storage.remove('checkoutInProgress')
                                                            .then(() => {
                                                            console.log('CHKOUT: STATUS -> Removed checkoutInProgress from storage.');
                                                            this.pageRedirect('home', 'Order checked out successfully!', 'toast', 2000, 2000, loading);
                                                        })
                                                            .catch((e) => {
                                                            console.log('CHKOUT: Error in removing checkoutInProgress from storage.');
                                                            console.log('CHKOUT: A-ERROR -> ' + e);
                                                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                        });
                                                    })
                                                        .catch((e) => {
                                                        console.log('CHKOUT: Error in updating orderList.');
                                                        console.log('CHKOUT: A-ERROR -> ' + e);
                                                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                    });
                                                }
                                                else {
                                                    // More items remaining in cart
                                                    console.log('Not all items checked out.');
                                                    if (this.network.type != 'none') {
                                                        console.log('Network available to continue.');
                                                        // Set attemptCount = 0 in storage
                                                        // Set 'checkoutInProgress: true' key-value pair in storage
                                                        this.storage.set('attempCount', 0)
                                                            .then(() => {
                                                            this.storage.set('checkoutInProgress', 'true')
                                                                .then(() => {
                                                                this.pageRedirect('cart', '', 'none', 0, 400, loading);
                                                            })
                                                                .catch((e) => {
                                                                console.log('Error in setting checkoutInProgress in storage.');
                                                                console.log('CHKOUT: A-ERROR -> ' + e);
                                                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                            });
                                                        })
                                                            .catch((e) => {
                                                            console.log('CHKOUT: Error in setting attemptCount in storage.');
                                                            console.log('CHKOUT: A-ERROR -> ' + e);
                                                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                        });
                                                    }
                                                    else {
                                                        console.log('CHKOUT: STATUS Ending exec -> Network not available to continue checkout.');
                                                        console.log('CHKOUT: STATUS Ending exec -> Updating orderList.');
                                                        this.updateOrderList(itemsCheckedOut)
                                                            .then((result) => {
                                                            // Remove 'checkoutInProgress' key-value pair from storage
                                                            this.storage.remove('checkoutInProgress')
                                                                .then(() => {
                                                                this.pageRedirect('cart', 'Network issues. Unable to checkout all items at this time.', 'toastok', 2000, 2000, loading);
                                                            })
                                                                .catch((e) => {
                                                                console.log('CHKOUT: Error in removing checkoutInProgress.');
                                                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                            });
                                                        })
                                                            .catch((e) => {
                                                            console.log('CHKOUT: A-ERROR -> ' + e);
                                                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                        });
                                                    }
                                                }
                                            })
                                                .catch((e) => {
                                                console.log('CHKOUT: A-ERROR -> ' + e);
                                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                            });
                                        }
                                        else {
                                            // Unsuccessful response from server
                                            console.log('CHKOUT: Failed response from server.');
                                            console.log('CHKOUT: ERROR -> Error ' + res.status + ': ' + res.error);
                                            // Read value stored in key 'attemptCount'
                                            // Increment attemptCount
                                            console.log('CHKOUT: Looking up attempt number for current entries.');
                                            this.storage.get('attemptCount')
                                                .then((val) => {
                                                let attemptCount = Number(val) + 1;
                                                this.storage.set('attemptCount', attemptCount)
                                                    .then(() => {
                                                    if (attemptCount > 10) {
                                                        console.log('CHKOUT: ERROR -> Multiple failed attempts.');
                                                        let itemsCheckedOut = this.cartsize - cartSize;
                                                        if (itemsCheckedOut > 0) {
                                                            console.log('CHKOUT: STATUS Ending exec -> Updating info in SQLite of checkout out items.');
                                                            this.updateOrderList(itemsCheckedOut)
                                                                .then((result) => {
                                                                // Remove storage variable 'checkoutInProgress' and redirect to cart page
                                                                this.storage.remove('checkoutInProgress')
                                                                    .then(() => {
                                                                    this.storage.remove('attemptCount')
                                                                        .then(() => {
                                                                        console.log('CHKOUT: STATUS Ending exec -> Removed storage variables checkoutInProgress, attemptCount.');
                                                                        this.pageRedirect('cart', 'Multiple failed attempts to write to server. Please try again later.', 'toastok', 2000, 1500, loading);
                                                                    })
                                                                        .catch((e) => {
                                                                        console.log('CHKOUT: ERROR -> Error in resetting attemptCount in storage.');
                                                                        console.log('CHKOUT: A-ERROR -> ' + e);
                                                                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                                    });
                                                                })
                                                                    .catch((e) => {
                                                                    console.log('CHKOUT: ERROR -> Error in removing checkoutInProgress from storage.');
                                                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                                });
                                                            })
                                                                .catch((e) => {
                                                                console.log('CHKOUT: A-ERROR -> ' + e);
                                                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                            });
                                                        }
                                                        else {
                                                            this.storage.remove('checkoutInProgress')
                                                                .then(() => {
                                                                this.storage.remove('attemptCount')
                                                                    .then(() => {
                                                                    console.log('CHKOUT: STATUS Ending exec -> Removed storage variables checkoutInProgress, attemptCount.');
                                                                    this.pageRedirect('cart', 'Multiple failed attempts to write to server. Please try again later.', 'toastok', 2000, 1500, loading);
                                                                })
                                                                    .catch((e) => {
                                                                    console.log('CHKOUT: Error in removing attemptCount.');
                                                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                                });
                                                            })
                                                                .catch((e) => {
                                                                console.log('CHKOUT: Error in removing checkoutInProgress.');
                                                                console.log('CHKOUT: A-ERROR -> ' + e);
                                                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                            });
                                                        }
                                                    }
                                                    else {
                                                        console.log('Resending request.');
                                                        console.log('CHKOUT: Attempting to send data to server again.');
                                                        // Set 'checkoutInProgress: true' key-value pair in storage
                                                        this.storage.set('checkoutInProgress', 'true')
                                                            .then(() => {
                                                            console.log('CHKOUT: STATUS Retry -> Set checkoutInProgress to true.');
                                                            this.pageRedirect('cart', '', 'none', 0, 500, loading);
                                                        })
                                                            .catch((e) => {
                                                            console.log('CHKOUT: Error in writing checkoutInProgress variable to storage.');
                                                            console.log('CHKOUT: A-ERROR -> ' + e);
                                                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                        });
                                                    }
                                                })
                                                    .catch((e) => {
                                                    console.log('CHKOUT: Error in updating attemptCount.');
                                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                });
                                            })
                                                .catch((e) => {
                                                console.log('AttemptCount not found in storage.');
                                                console.log(e);
                                                this.storage.set('attemptCount', 1)
                                                    .then(() => {
                                                    // Set 'checkoutInProgress: true' key-value pair in storage
                                                    this.storage.set('checkoutInProgress', 'true')
                                                        .then(() => {
                                                        console.log('CHKOUT: Attempting to send data to server again.');
                                                        this.pageRedirect('cart', '', 'none', 0, 500, loading);
                                                    })
                                                        .catch((e) => {
                                                        console.log('CHKOUT: Error in writing checkoutInProgress variable to storage.');
                                                        console.log('CHKOUT: A-ERROR -> ' + e);
                                                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                    });
                                                })
                                                    .catch((e) => {
                                                    console.log('CHKOUT: Error in setting attemptCount in storage.');
                                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                                });
                                            });
                                        }
                                    })
                                        .catch((e) => {
                                        console.log('CHKOUT: Error in decrypting data received.');
                                        console.log('CHKOUT: A-ERROR -> ' + e);
                                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                    });
                                })
                                    .catch((e) => {
                                    console.log(e);
                                    this.storage.remove('checkoutInProgress')
                                        .then(() => {
                                        this.pageRedirect('cart', 'Issue in request sent to server. Please report incident.', 'toastok', 2000, 1500, loading);
                                    })
                                        .catch((e) => {
                                        console.log('CHKOUT: Error in removing checkoutInProgress.');
                                        console.log('CHKOUT: A-ERROR -> ' + e);
                                        this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                    });
                                });
                            })
                                .catch((e) => {
                                console.log('CHKOUT: Error in connecting to server.');
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.storage.remove('checkoutInProgress')
                                    .then(() => {
                                    this.pageRedirect('cart', 'Issue in connecting to server. Please report incident.', 'toastok', 2000, 1500, loading);
                                })
                                    .catch((e) => {
                                    console.log('CHKOUT: Error in removing checkoutInProgress.');
                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                });
                            });
                        })
                            .catch((e) => {
                            console.log('CHKOUT: Error during encryption function call.');
                            console.log('CHKOUT: A-ERROR -> ' + e);
                            this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                        });
                    }
                    else {
                        console.log('CHKOUT: ERROR -> Fetched records, but no network to continue.');
                        // No network connection
                        let itemsCheckedOut = this.cartsize - cartSize;
                        if (itemsCheckedOut > 0) {
                            this.updateOrderList(itemsCheckedOut)
                                .then((result) => {
                                // Remove storage variable and redirect to cart page
                                this.storage.remove('checkoutInProgress')
                                    .then(() => {
                                    console.log('CHKOUT: STATUS Ending exec -> Removed checkoutInProgress from storage.');
                                    this.pageRedirect('cart', 'Network error. Unable to proceed at this point of time.', 'toastok', 0, 500, loading);
                                })
                                    .catch((e) => {
                                    console.log('CHKOUT: Error in removing checkoutInProgress.');
                                    console.log('CHKOUT: A-ERROR -> ' + e);
                                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                                });
                            })
                                .catch((e) => {
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                        }
                        else {
                            console.log('CHKOUT: STATUS Ending exec -> No checked out items to update in SQLite.');
                            this.storage.remove('checkoutInProgress')
                                .then(() => {
                                console.log('CHKOUT: STATUS Ending exec -> Removed checkoutInProgress from storage.');
                                this.pageRedirect('cart', 'Network error. Unable to proceed at this point of time.', 'toastok', 0, 2000, loading);
                            })
                                .catch((e) => {
                                console.log('CHKOUT: Error in removing checkoutInProgress.');
                                console.log('CHKOUT: A-ERROR -> ' + e);
                                this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                            });
                        }
                    }
                })
                    .catch((e) => {
                    console.log('CHKOUT: STATUS Ending exec -> Issue in fetching top 20 entries.');
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    this.pageRedirect('cart', 'Unexpected termination of execution', 'toast', 1500, 500, loading);
                });
            }
        }
    }
    getCartSize() {
        console.log('GETSZE: Call to getCartSize.');
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                let sql = 'SELECT COUNT(timestamp) as itemsincart FROM orderTable ' +
                    'WHERE submitted = 0 ' +
                    'AND user_id = ?';
                console.log('GETSZE: Initiate count items in cart from SQLite.');
                db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                    .then((result) => {
                    this.storage.set('cartsize', Number(result.rows.item(0).itemsincart))
                        .then(() => {
                        console.log('GETSZE: STATUS Ending exec -> Successfully updated.');
                        resolve('Success');
                    })
                        .catch((e) => {
                        reject(e);
                    });
                })
                    .catch((e) => {
                    reject(e);
                });
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    checkoutCart() {
        this.getCartSize()
            .then(() => {
            this.storage.get('cartsize')
                .then((val) => {
                this.cartsize = Number(val);
                console.log('Local value for cartsize updated to ' + this.cartsize + '.');
                setTimeout(() => {
                    this.checkoutProcess();
                }, 100);
            })
                .catch((e) => {
                console.log('Error in fetching original cart size.');
            });
        })
            .catch((e) => {
            console.log('Cannot proceed, issue with initiating cart size.');
        });
    }
    confirmCheckout(canProceed) {
        if (!canProceed) {
            // Show toast about empty cart
            let toast = this.toastCtrl.create({
                message: 'You do not have any items in your cart!',
                duration: 1500,
                position: 'middle'
            });
            toast.present();
        }
        else {
            let alertConfirm = this.alertCtrl.create({
                title: 'Submit Order',
                message: 'Do you want to submit order?',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel'
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            this.checkoutCart();
                        }
                    }
                ]
            });
            alertConfirm.present();
        }
    }
    getRecordsAsMailBody(platform) {
        console.log('CHKOUT: Call to getRecordsAsMailBody');
        //let body = "<h3>ORDER CONTENTS</h3><br>";
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                let sql = 'SELECT * FROM orderTable ' +
                    'WHERE submitted = 0 ' +
                    'AND user_id = ? ' +
                    'ORDER BY ponumber, timestamp';
                console.log('CHKOUT: Initiate fetching rows from SQLite');
                db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                    .then((result) => {
                    if (result.rows.length == 0) {
                        reject('CHKOUT: ERROR in getRecordsAsMailBody -> No rows found.');
                    }
                    else {
                        console.log('CHKOUT: Building email body.');
                        if (platform == 'android') {
                            let body = "ORDER CONTENTS\r\n\r\n" +
                                "Order created by " + __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].username + "\r\n\r\n";
                            // List form - only Android
                            for (let i = 0; i < result.rows.length; i++) {
                                body += "Item " + (i + 1) + "\r\n" +
                                    "Ship to ID: " + result.rows.item(i).shiptoid + "\r\n" +
                                    "Contact ID: " + result.rows.item(i).contactid + "\r\n" +
                                    "PO Number: " + result.rows.item(i).ponumber + "\r\n" +
                                    "Tag ID: " + result.rows.item(i).tagid + "\r\n" +
                                    "Quantity: " + result.rows.item(i).quantity + "\r\n" +
                                    "Extended descr.: " + result.rows.item(i).extdesc + "\r\n\r\n";
                                if (i == (result.rows.length - 1)) {
                                    console.log('CHKOUT: Completed building email body.');
                                    resolve(body);
                                }
                            }
                        }
                        else if (platform == 'ios') {
                            // Tabular form - only iOS
                            let body = "<h3>ORDER CONTENTS</h3><br> " +
                                "<p>Order created by: <b>" + __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].username + "</b><br><br><p>" +
                                "<table style=\"width: 100%; border-width: 2px; border-style: solid; border-color: #000000; border-collapse: collapse;\"> " +
                                "<tr> " +
                                " <td style=\"width: 7%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>#</b></td> " +
                                " <td style=\"width: 20%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Ship to ID</b></td> " +
                                " <td style=\"width: 15%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Contact ID</b></td> " +
                                " <td style=\"width: 20%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>PO Number</b></td> " +
                                " <td style=\"width: 25%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Tag ID</b></td> " +
                                " <td style=\"width: 13%; padding: 1%; text-align: center; border-width: 1px; border-style: solid; border-color: #000000;\"><b>Quantity</b></td> " +
                                "</tr> ";
                            for (let i = 0; i < result.rows.length; i++) {
                                body += "<tr> " +
                                    " <td rowspan=\"2\" style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; text-align: center; border-width: 1px; border-style:solid; border-color: #696969;\">" + (i + 1) + "</td>" +
                                    " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).shiptoid + "</td>" +
                                    " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).contactid + "</td>" +
                                    " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).ponumber + "</td>" +
                                    " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).tagid + "</td>" +
                                    " <td style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969; border-bottom-color: #a9a9a9\">" + result.rows.item(i).quantity + "</td>" +
                                    " </tr>" +
                                    "<tr>" +
                                    " <td colspan=\"5\" style=\"padding: 0.3%; padding-left: 0.5%; padding-right: 0.5%; border-width: 1px; border-style:solid; border-color: #696969;\"> Ext desc: " + result.rows.item(i).extdesc + "</td>" +
                                    "</tr>";
                                if (i == (result.rows.length - 1)) {
                                    console.log('CHKOUT: Completed building email body.');
                                    resolve(body + "</table>");
                                }
                            }
                        }
                    }
                })
                    .catch((e) => {
                    reject(e);
                });
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    sendAsMail(platform) {
        let loading = this.loadingCtrl.create({
            content: 'Sending as email...'
        });
        loading.present();
        /*
        this.emailComposer.isAvailable()
        .then((available: boolean) => {
          if(available) {
            */
        console.log('CHKOUT: Email client unavailable.');
        this.getRecordsAsMailBody(platform)
            .then((body) => {
            let email;
            if (platform == 'android') {
                email = {
                    subject: 'Air Hydro Items Order',
                    body: body.toString(),
                    isHtml: false
                };
            }
            else if (platform == 'ios') {
                email = {
                    //to: '',
                    subject: 'Air Hydro Items Order',
                    body: body.toString(),
                    isHtml: true
                };
            }
            setTimeout(() => {
                this.emailComposer.open(email)
                    .then(() => {
                    console.log('CHKOUT: Email client opened.');
                    loading.dismiss();
                    // Order sent as email
                    //this.pageRedirect('cart', 'Items checked out as email', 'toast', 1500, 500, loading);
                })
                    .catch((e) => {
                    console.log('CHKOUT: A-ERROR -> ' + e);
                    console.log('CHKOUT: ERROR -> Unable to open email client.');
                    loading.dismiss();
                });
            }, 200);
        })
            .catch((e) => {
            console.log('CHKOUT: A-ERROR -> ' + e);
            console.log('CHKOUT: ERROR -> Issue in getting rows as email body.');
            loading.dismiss();
        });
        /*
        }
        else {
          console.log('CHKOUT: ERROR -> Email client unavailable.');
          loading.dismiss();
        }
      })
      .catch((e) => {
        console.log('CHKOUT: A-ERROR -> ' + e);
        console.log('CHKOUT: ERROR -> Error in accessing Email Composer.');
        loading.dismiss();
      });
      */
    }
    setItems() {
        return new Promise((resolve, reject) => {
            this.orderListWhole.length = 0;
            this.platform.ready().then(() => {
                this.sqlite.create({
                    name: __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].db,
                    location: 'default'
                })
                    .then((db) => {
                    let sql = 'SELECT * FROM orderTable ' +
                        'WHERE submitted = 0 ' +
                        'AND user_id = ?';
                    db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                        .then((result) => {
                        for (let i = 0; i < result.rows.length; i++) {
                            new Promise((resolve) => {
                                let orderElement = [
                                    result.rows.item(i).shiptoid,
                                    result.rows.item(i).contactid,
                                    result.rows.item(i).ponumber,
                                    result.rows.item(i).tagid,
                                    result.rows.item(i).quantity,
                                    result.rows.item(i).extdesc,
                                    result.rows.item(i).timestamp
                                ];
                                resolve(orderElement);
                            })
                                .then((orderElement) => {
                                this.orderListWhole.push([orderElement[0], orderElement[1], orderElement[2], orderElement[3], orderElement[4], orderElement[5], orderElement[6]]);
                                this.orderList.push([orderElement[0], orderElement[1], orderElement[2], orderElement[3], orderElement[4], orderElement[5], orderElement[6]]);
                            })
                                .then(() => {
                                if (i == (result.rows.length - 1)) {
                                    resolve();
                                }
                            })
                                .catch((e) => {
                                reject(e);
                            });
                        }
                    })
                        .catch((e) => {
                        reject(e);
                    });
                })
                    .catch((e) => {
                    reject(e);
                });
            })
                .catch((e) => {
                reject(e);
            });
        });
    }
    resetItems() {
        this.orderList.length = 0;
        for (var i = 0; i < this.orderListWhole.length; i++) {
            this.orderList.push(this.orderListWhole[i]);
        }
    }
    deleteItem(target) {
        // SQLite
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then((db) => {
            let sql = 'DELETE FROM orderTable ' +
                'WHERE timestamp = ? AND submitted = 0 AND user_id = ?';
            db.executeSql(sql, [target, __WEBPACK_IMPORTED_MODULE_9__app_global__["a" /* Global */].userid])
                .then(() => {
                console.log('Entry deleted');
                this.getCartSize()
                    .then(() => {
                    this.events.publish('menu:refresh');
                    this.navCtrl.setRoot(CartPage_1);
                })
                    .catch((e) => {
                    console.log(e);
                });
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    confirmDelete(target) {
        let alertConfirm = this.alertCtrl.create({
            title: 'Delete Item',
            message: 'Do you want to delete chosen item?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel'
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.deleteItem(target);
                    }
                }
            ]
        });
        alertConfirm.present();
    }
    filterCartItems(ev) {
        this.resetItems();
        let val = ev.target.value;
        if (val && val.trim() !== '') {
            this.orderList = this.orderList.filter((item) => {
                let isMatching = false;
                for (let i = 0; i < item.length; i++) {
                    if (item[i].toString().toLowerCase().indexOf(val.toLowerCase()) > (-1)) {
                        isMatching = true;
                        break;
                    }
                }
                return isMatching;
            });
        }
    }
    pageRedirect(target, message, alerttype, showtime, waittime, loading) {
        if (target == 'order') {
            if (alerttype == 'toast') {
                let toast = this.toastCtrl.create({
                    message: message,
                    duration: showtime,
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Order Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__order_order__["b" /* OrderPage */]);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'toastok') {
                let toast = this.toastCtrl.create({
                    message: message,
                    showCloseButton: true,
                    closeButtonText: 'OK',
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Order Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__order_order__["b" /* OrderPage */]);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'none') {
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Order Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__order_order__["b" /* OrderPage */]);
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
        }
        else if (target == 'cart') {
            if (alerttype == 'toast') {
                let toast = this.toastCtrl.create({
                    message: message,
                    duration: showtime,
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Cart Page');
                    this.navCtrl.setRoot(CartPage_1);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'toastok') {
                let toast = this.toastCtrl.create({
                    message: message,
                    showCloseButton: true,
                    closeButtonText: 'OK',
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Cart Page');
                    this.navCtrl.setRoot(CartPage_1);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'none') {
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Cart Page');
                    this.navCtrl.setRoot(CartPage_1);
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
        }
        else if (target == 'home') {
            if (alerttype == 'toast') {
                let toast = this.toastCtrl.create({
                    message: message,
                    duration: showtime,
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Home Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'toastok') {
                let toast = this.toastCtrl.create({
                    message: message,
                    showCloseButton: true,
                    closeButtonText: 'OK',
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Home Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
                    toast.present();
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
            else if (alerttype == 'none') {
                setTimeout(() => {
                    this.events.publish('menu:refresh');
                    console.log('REDIRECT: Redirecting to Home Page');
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__home_home__["a" /* HomePage */]);
                    setTimeout(() => {
                        loading.dismiss();
                    }, 100);
                }, waittime);
            }
        }
    }
};
CartPage = CartPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-cart',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/cart/cart.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span [hidden]="deviceWidth < 576">\n        Menu\n      </span>\n    </button>\n    <ion-title>My Cart</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-end outline showWhen="ios" [hidden]="orderList.length == 0" (click)="sendAsMail(\'ios\')">\n        <ion-icon ios="ios-mail-outline" md="md-mail"></ion-icon> &nbsp; Send&nbsp;\n      </button>\n      <button ion-button icon-end outline showWhen="android" [hidden]="orderList.length == 0" (click)="sendAsMail(\'android\')">\n        <ion-icon ios="ios-mail-outline" md="md-mail"></ion-icon> &nbsp; Send&nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-toolbar>\n    <ion-searchbar placeholder="Search in cart" (ionInput)="filterCartItems($event)"></ion-searchbar>\n  </ion-toolbar>\n\n  <ion-list *ngIf="orderList.length > 0">\n    <ion-item *ngFor="let item of orderList">\n        <div id="item_info">\n          <p>\n            Ship to ID: <b>{{ item[0] }}</b><br>\n            Contact ID: <b>{{ item[1] }}</b><br>\n            PO Number: <b>{{ item[2] }}</b><br>\n            Tag ID: <b>{{ item[3] }}</b><br>\n            Quantity: <b>{{ item[4] }}</b><br>\n            Ext Descr: <b>{{ item[5] }}</b><br>\n          </p>\n        </div>\n        <div id="item_delete">\n          <a href="#" (click)="confirmDelete(item[6])">\n            Remove &nbsp;<ion-icon name="trash"></ion-icon>\n          </a>\n        </div>\n    </ion-item>\n  </ion-list>\n\n  <p *ngIf="orderList.length == 0" style="padding: 5%; text-align: center;">\n    No items in your cart yet.\n  </p>\n</ion-content>\n\n<ion-footer style="display: inline;">\n  <button ion-button color="light" (click)="addMore()" style="float: left; width: 48%; margin-left: 1%; margin-top: 0.5%; margin-bottom: 0.5%">\n    <ion-icon name="add"></ion-icon>&nbsp; ADD MORE\n  </button>\n  <button id="checkout_button" [hidden]="orderList.length == 0" (click)="confirmCheckout(true)" ion-button style="float: right; width: 48%; margin-right: 1%; margin-top: 0.5%; margin-bottom: 0.5%">\n    SUBMIT\n  </button>\n  <button id="inactive_checkout_button" [hidden]="orderList.length > 0" (click)="confirmCheckout(false)" ion-button style="float: right; width: 48%; margin-right: 1%; margin-top: 0.5%; margin-bottom: 0.5%">\n    SUBMIT\n  </button>\n</ion-footer>\n'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/cart/cart.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__["a" /* HTTP */],
        __WEBPACK_IMPORTED_MODULE_11__providers_cryptprovider__["a" /* Encryption */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_email_composer__["a" /* EmailComposer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], CartPage);

var CartPage_1;
//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectionVariables; });
var ConnectionVariables = {
    httpUrl: 'https://mim.airhydropower.com/airhydromobile/airhydrorddemo.php',
    //httpUrl: 'http://localhost/airhydrorddemo.php',
    timeout: 10
};
//# sourceMappingURL=connectionVariables.js.map

/***/ }),

/***/ 121:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 121;

/***/ }),

/***/ 163:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 163;

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let DataProvider = class DataProvider {
    constructor(sqlite) {
        this.sqlite = sqlite;
    }
    getData() {
        // Return mock data asynchronously
        let data = [];
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then((db) => {
            let sql = 'SELECT * FROM orderList';
            db.executeSql(sql, {})
                .then((result) => {
                for (let i = 0; i < result.rows.length; i++) {
                    let orderinfo = [
                        result.rows.item(i).order_id,
                        result.rows.item(i).timestamp,
                        result.rows.item(i).size
                    ];
                    data.push(orderinfo);
                }
            });
        })
            .catch((e) => {
            console.log(e);
        });
        return data;
    }
    getAsyncData() {
        // Async receive mock data
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(this.getData());
            }, 1000);
        });
    }
};
DataProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */]])
], DataProvider);

//# sourceMappingURL=dataprovider.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_global__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_connectionVariables__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_cryptprovider__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









let ProfilePage = class ProfilePage {
    constructor(navCtrl, platform, app, loadingCtrl, toastCtrl, sqlite, storage, http, crypt, alertCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.sqlite = sqlite;
        this.storage = storage;
        this.http = http;
        this.crypt = crypt;
        this.alertCtrl = alertCtrl;
        this.devicewidth = this.platform.width();
        this.loadLastValues();
    }
    loadLastValues() {
        this.username = __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].username;
        this.email = __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].email;
    }
    logout() {
        let alertConfirm = this.alertCtrl.create({
            title: 'Log Out',
            message: 'Are you sure you want to log out?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel'
                },
                {
                    text: 'Yes',
                    handler: () => {
                        // Reset any global variables
                        let loading = this.loadingCtrl.create({
                            content: 'Logging out...'
                        });
                        let toast = this.toastCtrl.create({
                            message: 'You have succesfully logged out!',
                            duration: 1800,
                            position: 'bottom'
                        });
                        //let link = 'http://127.0.0.1/airhydrodemo.php';
                        let data = {
                            'action': 'logout',
                            'username': __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].username
                        };
                        this.http.setRequestTimeout(__WEBPACK_IMPORTED_MODULE_6__app_connectionVariables__["a" /* ConnectionVariables */].timeout);
                        this.http.acceptAllCerts(true)
                            .then(() => {
                            this.http.post(__WEBPACK_IMPORTED_MODULE_6__app_connectionVariables__["a" /* ConnectionVariables */].httpUrl, data, {})
                                .then((result) => {
                                let res = JSON.parse(result.data);
                                if (res.status == 1) {
                                    // Logout successful
                                    console.log('Logout request executed successfully at server.' + res.response);
                                    this.sqlite.create({
                                        name: __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].db,
                                        location: 'default'
                                    })
                                        .then((db) => {
                                        let sql = 'UPDATE userProfiles ' +
                                            'SET status = 0 ' +
                                            'WHERE user_id = ?';
                                        db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].userid])
                                            .then(() => {
                                            __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].userid = 0;
                                            __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].email = '';
                                            __WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].username = '';
                                            this.storage.remove('userid')
                                                .catch((e) => {
                                                console.log('No variable userid to remove from storage.');
                                            });
                                            this.storage.remove('email')
                                                .catch((e) => {
                                                console.log('No variable email to remove from storage.');
                                            });
                                            this.storage.get('rememberme')
                                                .then((rememberme) => {
                                                if (!rememberme) {
                                                    console.log('Rememberme not set.');
                                                    this.storage.remove('username')
                                                        .catch((e) => {
                                                        console.log('No variable username to remove from storage.');
                                                    });
                                                    this.storage.remove('password')
                                                        .catch((e) => {
                                                        console.log('No variable password to remove from storage.');
                                                    });
                                                    this.storage.remove('rememberme')
                                                        .catch((e) => {
                                                        console.log('No variable rememberme to remove from storage.');
                                                    });
                                                }
                                            })
                                                .catch((e) => {
                                                console.log('Rememberme not found. Removing username & password.');
                                                this.storage.remove('username')
                                                    .catch((e) => {
                                                    console.log('No variable username to remove from storage.');
                                                });
                                                this.storage.remove('password')
                                                    .catch((e) => {
                                                    console.log('No variable password to remove from storage.');
                                                });
                                            });
                                            console.log('UserProfiles updated.');
                                            loading.present();
                                            setTimeout(() => {
                                                this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_7__login_login__["b" /* LoginPage */]);
                                                loading.dismiss();
                                                toast.present();
                                            }, 1000);
                                        })
                                            .catch((e) => {
                                            console.log(e);
                                            console.log('UserProfile status update failed.');
                                        });
                                    })
                                        .catch((e) => {
                                        console.log(e);
                                    });
                                }
                                else {
                                    // Logout unsuccessful
                                    console.log('Unable to log out.');
                                }
                            })
                                .catch((e) => {
                                console.log('Error in sending request to backend.');
                                console.log(e);
                            });
                        })
                            .catch((e) => {
                            console.log('Error in connecting to backend.');
                            console.log(e);
                        });
                    }
                }
            ]
        });
        alertConfirm.present();
    }
};
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span [hidden]="devicewidth < 576">\n        Menu\n      </span>\n    </button>\n    <ion-title>Profile</ion-title>\n    <ion-buttons end [hidden]="devicewidth < 576">\n      <button ion-button icon-end outline (click)="logout()">\n        &nbsp; Log Out <ion-icon name="log-out"></ion-icon> &nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  \n</ion-header>\n\n<ion-content padding>\n\n  <ion-list>\n    <ion-list-header>\n      Personal Info\n    </ion-list-header>\n    <ion-item>\n      <ion-label>\n        Username\n      </ion-label>\n      <ion-note item-end style="width: 70%; text-align: right;">\n        {{ username }}\n      </ion-note>\n    </ion-item>\n\n    <ion-list-header>\n      Contact Info\n    </ion-list-header>\n    <ion-item>\n      <ion-label>\n        Email\n      </ion-label>\n      <ion-note item-end style="width: 70%; text-align: right; font-size: 100%;">\n        {{ email }}\n      </ion-note>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer [hidden]="devicewidth > 576">\n  <div id="logout_button">\n    <button ion-button block style="text-align: center;" (click)="logout()"> <ion-icon name="log-out"></ion-icon> &nbsp; Log Out</button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/profile/profile.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_http__["a" /* HTTP */],
        __WEBPACK_IMPORTED_MODULE_8__providers_cryptprovider__["a" /* Encryption */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let AboutPage = class AboutPage {
    constructor(navCtrl, platform) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.deviceWidth = this.platform.width();
    }
};
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span [hidden]="deviceWidth < 576">\n        Menu\n      </span>\n    </button>\n    <ion-title>App Info</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-label>\n        Version\n      </ion-label>\n      <ion-note item-end>\n        1.0.0\n      </ion-note>\n    </ion-item>\n    <ion-item>\n      <ion-label>\n        Build Date\n      </ion-label>\n      <ion-note item-end>\n        July 03, 2018\n      </ion-note>\n    </ion-item>\n    <ion-item>\n      <ion-label>\n        Developer\n      </ion-label>\n      <ion-note item-end>\n        Comparatio USA\n      </ion-note>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/about/about.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(233);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_email_composer__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_order_order__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_cart_cart__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_about_about__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_dataprovider__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_network__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_cryptprovider__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















let AppModule = class AppModule {
};
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_order_order__["b" /* OrderPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["b" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_cart_cart__["a" /* CartPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_order_order__["a" /* OrderInfoModal */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* ErrorMessageModal */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_order_order__["b" /* OrderPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["b" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_cart_cart__["a" /* CartPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_order_order__["a" /* OrderInfoModal */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* ErrorMessageModal */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_15__providers_dataprovider__["a" /* DataProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_network__["a" /* Network */],
            Storage,
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_19__providers_cryptprovider__["a" /* Encryption */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_email_composer__["a" /* EmailComposer */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_order_order__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_cart_cart__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_about_about__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__global__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













let MyApp = class MyApp {
    constructor(platform, statusBar, splashScreen, storage, sqlite, events) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.sqlite = sqlite;
        this.events = events;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["b" /* LoginPage */];
        this.initializeApp();
        this.refreshMenu();
        events.subscribe('menu:refresh', () => {
            this.refreshMenu();
        });
        // used for an example of ngFor and navigation
        // Controls the navigation menu
        this.pages = [
            { title: 'My Orders', icon: 'list', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */] },
            { title: 'Build Order', icon: 'create', component: __WEBPACK_IMPORTED_MODULE_6__pages_order_order__["b" /* OrderPage */] },
            { title: 'My Cart', icon: 'cart', component: __WEBPACK_IMPORTED_MODULE_8__pages_cart_cart__["a" /* CartPage */] },
            { title: 'Profile', icon: 'contact', component: __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */] },
            { title: 'About', icon: 'information-circle', component: __WEBPACK_IMPORTED_MODULE_10__pages_about_about__["a" /* AboutPage */] }
        ];
    }
    refreshMenu() {
        // Get number of cart entries
        // SQLite read
        console.log('Reading from SQLite.');
        let dbname = 'data.db';
        this.sqlite.create({
            name: dbname,
            location: 'default'
        })
            .then((db) => {
            // Fetch number of items in cart
            let sql = 'SELECT COUNT(timestamp) as itemsincart FROM orderTable ' +
                'WHERE submitted = 0 ' +
                'AND user_id = ?';
            db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_12__global__["a" /* Global */].userid])
                .then((result) => {
                this.cartSize = result.rows.item(0).itemsincart;
                //alert(this.cartSize);
                console.log('Updated cart size.');
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.createTables();
            this.refreshMenu();
        });
    }
    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
    createTables() {
        console.log('APPINI: Creating/opening database');
        this.platform.ready().then(() => {
            this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then((db) => {
                console.log('APPINI: Creating tables, if not present');
                let sql = 'CREATE TABLE IF NOT EXISTS orderTable (' +
                    'timestamp VARCHAR(128) PRIMARY KEY, ' +
                    'shiptoid INTEGER, ' +
                    'contactid INTEGER, ' +
                    'ponumber VARCHAR(255), ' +
                    'tagid VARCHAR(128), ' +
                    'quantity INTEGER, ' +
                    'extdesc VARCHAR(255), ' +
                    'submitted TINYINT(2) DEFAULT 0, ' +
                    'user_id INTEGER)';
                db.executeSql(sql, {})
                    .then(() => {
                    console.log('APPINI: Status -> OrderTable table created.');
                    //alert('OrderTable table created');
                    sql = 'CREATE TABLE IF NOT EXISTS orderList (' +
                        'order_id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
                        'timestamp DATETIME, ' +
                        'size INTEGER NOT NULL, ' +
                        'user_id INTEGER)';
                    db.executeSql(sql, {})
                        .then(() => {
                        console.log('APPINI: Status -> OrderList table created.');
                        //alert('OrderList table created');
                        sql = 'CREATE TABLE IF NOT EXISTS userProfiles (' +
                            'user_id INTEGER PRIMARY KEY, ' +
                            'username VARCHAR(128), ' +
                            'email VARCHAR(128), ' +
                            'status TINYINT(2) DEFAULT 0, ' +
                            'lastlogin DATETIME)'; // 1 is logged in, 0 is logged out
                        db.executeSql(sql, {})
                            .then(() => {
                            console.log('APPINI: Status -> UserProfiles table created.');
                        })
                            .catch((e) => {
                            console.log('APPINI: A-ERROR -> ' + e);
                            console.log('APPINI: Error -> UserProfiles table creation failed.');
                        });
                    })
                        .catch((e) => {
                        console.log('APPINI: A-ERROR -> ' + e);
                        console.log('APPINI: Error -> OrderList table creation failed.');
                        //alert('OrderList table creation failed.');
                    });
                })
                    .catch((e) => {
                    console.log('APPINI: A-ERROR -> ' + e);
                    console.log('APPINI: Error -> OrderTable table creation failed.');
                });
            })
                .catch((e) => {
                console.log('APPINI: A-ERROR -> ' + e);
                console.log('APPINI: Error -> Database access error.');
            });
        })
            .catch((e) => {
            console.log('APPINI: A-ERROR -> ' + e);
            console.log('APPINI: Error -> Platform not ready.');
        });
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>My Air Hydro RD</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        <div style="display: inline; vertical-align: middle;">\n          <ion-icon [name]="p.icon" style="vertical-align: middle; width: 7%;"></ion-icon> &nbsp;\n          <h2 style="display: inline; vertical-align: middle; padding-left: 3%;">{{p.title}} &nbsp; </h2>\n          <!--<button id="cartsize" color="danger" *ngIf="itemsincart.length > 0 && p.icon == \'cart\'">-->\n          <button id="cartsize" color="danger" *ngIf="cartSize > 0 && p.icon == \'cart\'">\n            {{ cartSize }}\n          </button>\n          <ion-icon md="md-arrow-forward" style="float: right;"></ion-icon>\n        </div>\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="true"></ion-nav>\n'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Global; });
var Global = {
    userid: 0,
    username: '',
    db: 'data.db',
    email: '',
    accesslimit: 1440
};
//# sourceMappingURL=global.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return OrderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderInfoModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cart_cart__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_global__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









let OrderPage = OrderPage_1 = class OrderPage {
    constructor(navCtrl, navParams, barcodeScanner, storage, loadingCtrl, events, modalCtrl, toastCtrl, sqlite, alertCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcodeScanner = barcodeScanner;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.sqlite = sqlite;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.shiptoid = '';
        this.contactid = '';
        this.ponumber = '';
        this.tagid = '';
        this.quantity = '';
        this.extdesc = '';
        this.devicewidth = this.platform.width();
        this.fillLastValues();
    }
    addToCart() {
        // Check if all necessary fields are complete
        // Not being checked through 'required'
        if (this.shiptoid == null || this.shiptoid == '' || this.shiptoid == undefined
            || this.tagid == null || this.tagid == '' || this.tagid == undefined
            || this.quantity == null || this.quantity == '' || this.quantity == undefined) {
            // Return focus back to correct input item in form - not working
            alert('Please enter all required values correctly.');
            this.openModal();
            return;
        }
        else if (this.shiptoid.toString().length > 11) {
            alert('Ship to ID must be less than 10 digits.');
            this.openModal();
            return;
        }
        else if (this.contactid.toString().length > 10) {
            alert('Contact ID must be less than 10 digits.');
            this.openModal();
            return;
        }
        //Store values in local storage
        let valuestosave = {};
        valuestosave = [this.shiptoid, this.contactid, this.ponumber];
        this.saveLastValues(valuestosave);
        // Set to storage (timstamp, values-array)
        let timestamp;
        timestamp = new Date();
        timestamp = timestamp.toString();
        // SQLite
        this.platform.ready()
            .then(() => {
            this.sqlite.create({
                name: __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].db,
                location: 'default'
            })
                .then((db) => {
                // Check if existing entry is present
                let sql = 'SELECT * FROM orderTable ' +
                    'WHERE shiptoid = ? ' +
                    'AND contactid = ? ' +
                    'AND ponumber = ? ' +
                    'AND tagid = ?';
                db.executeSql(sql, [this.shiptoid, this.contactid, this.ponumber, this.tagid])
                    .then((result) => {
                    if (result.rows.length > 0) {
                        alert('You have already scanned this item for same Ship to ID, Contact ID and PO Number.');
                        return;
                    }
                    else {
                        // Insert new item into cart
                        sql = 'INSERT INTO orderTable(timestamp, shiptoid, contactid, ponumber, tagid, quantity, extdesc, submitted, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
                        db.executeSql(sql, [timestamp, this.shiptoid, this.contactid, this.ponumber, this.tagid, this.quantity, this.extdesc, 0, __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid])
                            .then(() => {
                            console.log('Entry inserted into table');
                            this.events.publish('menu:refresh');
                            // Old approach - locally creating a controller
                            // Improved approach - have a provider and create its instance to call loader - not working
                            let loading = this.loadingCtrl.create({
                                content: 'Processing...'
                            });
                            loading.present();
                            setTimeout(() => {
                                this.navCtrl.setRoot(OrderPage_1);
                                let toast = this.toastCtrl.create({
                                    message: 'Item added to cart',
                                    duration: 1000,
                                    position: 'middle'
                                });
                                loading.dismiss();
                                toast.present();
                            }, 200);
                        })
                            .catch((e) => {
                            console.log(e);
                        });
                    }
                })
                    .catch((e) => {
                    console.log(e);
                });
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    fillLastValues() {
        this.storage.get('last')
            .then((element) => {
            this.shiptoid = element[0];
            this.contactid = element[1];
            this.ponumber = element[2];
            setTimeout(() => {
                this.TagID.setFocus();
                console.log('INIT: Previous values loaded.');
            }, 1000);
        })
            .catch((e) => {
            console.log('INIT: No last values found.');
            setTimeout(() => {
                this.ShiptoID.setFocus();
            }, 200);
        });
    }
    openModal() {
        let modal = this.modalCtrl.create(OrderInfoModal);
        modal.present();
    }
    pageRedirect(target) {
        if (target == 'cart') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__cart_cart__["a" /* CartPage */]);
        }
    }
    saveLastValues(valueset) {
        console.log('ADDCRT: Saving form values to remember as \'last\'');
        this.storage.remove('last');
        let key = 'last';
        this.storage.set(key, valueset);
    }
    scan() {
        this.options = {
            showFlipCameraButton: false,
            preferFrontCamera: false,
            prompt: 'Scan a barcode',
            resultDisplayDuration: 200
        };
        return new Promise(resolve => {
            this.platform.ready()
                .then(() => {
                this.barcodeScanner.scan(this.options).then((result) => {
                    if (!result.cancelled) {
                        resolve(result.text);
                    }
                }, (error) => {
                    resolve('');
                });
            });
        });
    }
    scanCode(target) {
        this.scan()
            .then(result => {
            console.log('SCNFLD: Scan success.');
            if (target == 'shiptoid') {
                // If not a number, do not set value and provide toast
                if (isNaN(Number(result))) {
                    // Generate toast
                    let toast = this.toastCtrl.create({
                        message: 'Ship to ID needs to be a number only.',
                        position: 'bottom',
                        showCloseButton: true,
                        closeButtonText: 'OK'
                    });
                    toast.present();
                }
                else {
                    this.shiptoid = result;
                    this.shiptoid = this.shiptoid.trim();
                }
            }
            else if (target == 'contactid') {
                // If not a number, do not set value and provide toast
                if (isNaN(Number(result))) {
                    // Generate toast
                    let toast = this.toastCtrl.create({
                        message: 'Contact ID needs to be a number only.',
                        position: 'bottom',
                        showCloseButton: true,
                        closeButtonText: 'OK'
                    });
                    toast.present();
                }
                else {
                    this.contactid = result;
                    this.contactid = this.contactid.trim();
                }
            }
            else if (target == 'ponumber') {
                this.ponumber = result;
                this.ponumber = this.ponumber.trim();
            }
            else if (target == 'tagid') {
                this.tagid = result;
                this.tagid = this.tagid.trim();
            }
            else if (target == 'quantity') {
                // If not a number, do not set value and provide toast
                if (isNaN(Number(result))) {
                    // Generate toast
                    let toast = this.toastCtrl.create({
                        message: 'Quantity needs to be a number only.',
                        position: 'bottom',
                        showCloseButton: true,
                        closeButtonText: 'OK'
                    });
                    toast.present();
                }
                else {
                    this.quantity = result;
                    this.quantity = this.quantity.trim();
                }
            }
        })
            .catch((e) => {
            console.log('Scan error: ' + e);
        });
    }
};
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('shiptoidid'),
    __metadata("design:type", Object)
], OrderPage.prototype, "ShiptoID", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('contactidid'),
    __metadata("design:type", Object)
], OrderPage.prototype, "ContactID", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('ponumberid'),
    __metadata("design:type", Object)
], OrderPage.prototype, "PONumber", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('tagidid'),
    __metadata("design:type", Object)
], OrderPage.prototype, "TagID", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('quantity'),
    __metadata("design:type", Object)
], OrderPage.prototype, "Quantity", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('extdescid'),
    __metadata("design:type", Object)
], OrderPage.prototype, "ExtDesc", void 0);
OrderPage = OrderPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-order',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/order/order.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span [hidden]="devicewidth < 576">\n        Menu\n      </span>\n    </button>\n    <ion-title>Build Order</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-end outline (click)="pageRedirect(\'cart\')">\n        &nbsp; Cart <ion-icon name="cart"></ion-icon> &nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <!--\n    Ship to ID\n    Required\n  -->\n\n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n      <div style="width: 78%; float: left; height: auto; margin: 1%;">\n        <ion-item>\n          <ion-label stacked>\n            Ship to ID\n          </ion-label>\n          <ion-input #shiptoidid type="number" pattern="[0-9]*" [(ngModel)]="shiptoid" name="shiptoid" clearInput="true" placeholder="Enter Ship to ID" (keyup.enter)="ContactID.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 18%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'shiptoid\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon>\n        </button>\n      </div>\n    </div>\n  \n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n      <div style="width: 68%; float: left; height: 50px; margin: 1%;">\n        <ion-item>\n          <ion-label style="width: 20%">\n            Ship to ID\n          </ion-label>\n          <ion-input #shiptoidid type="number" pattern="[0-9]*" [(ngModel)]="shiptoid" name="shiptoid" clearInput="true" placeholder="Enter Ship to ID here" (keyup.enter)="ContactID.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 28%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'shiptoid\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon><p>&nbsp; Scan Ship to ID</p>\n        </button>\n      </div>\n    </div>\n\n  <!--\n    Contact ID\n    Not required\n  -->\n\n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n      <div style="width: 78%; float: left; height: auto; margin: 1%;">\n        <ion-item>\n          <ion-label stacked>\n            Contact ID\n          </ion-label>\n          <ion-input #contactidid type="number" pattern="[0-9]*" [(ngModel)]="contactid" name="contactid" clearInput="true" placeholder="Enter Contact ID" (keyup.enter)="PONumber.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 18%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'contactid\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon>\n        </button>\n      </div>\n    </div>\n  \n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n      <div style="width: 68%; float: left; height: 50px; margin: 1%;">\n        <ion-item>\n          <ion-label style="width: 20%">\n            Contact ID\n          </ion-label>\n          <ion-input #contactidid type="number" pattern="[0-9]*" [(ngModel)]="contactid" name="contactid" clearInput="true" placeholder="Enter Contact ID here" (keyup.enter)="PONumber.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 28%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'contactid\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon><p>&nbsp; Scan Contact ID</p>\n        </button>\n      </div>\n    </div>\n\n  <!--\n    PO Number\n    Not required\n  -->\n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n      <div style="width: 78%; float: left; height: auto; margin: 1%;">\n        <ion-item>\n          <ion-label stacked>\n            PO Number\n          </ion-label>\n          <ion-input #ponumberid type="text" [(ngModel)]="ponumber" name="ponumber" clearInput="true" placeholder="Enter PO Number" (keyup.enter)="TagID.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 18%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'ponumber\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon>\n        </button>\n      </div>\n    </div>\n\n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n      <div style="width: 68%; float: left; height: 50px; margin: 1%;">\n        <ion-item>\n          <ion-label style="width: 20%">\n            PO Number\n          </ion-label>\n          <ion-input #ponumberid type="text" [(ngModel)]="ponumber" name="ponumber" clearInput="true" placeholder="Enter PO Number here" (keyup.enter)="TagID.setFocus()"></ion-input>\n        </ion-item>\n      </div>\n      <div style="width: 28%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n        <button ion-button color="light" (click)="scanCode(\'ponumber\')" style="text-align: center; width: 95%; height: 100%;">\n          <ion-icon name="md-barcode"></ion-icon><p>&nbsp; Scan PO Number</p>\n        </button>\n      </div>\n    </div>\n\n  <!--\n    Tag ID (Item ID)\n    Required\n  -->\n\n  <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n    <div style="width: 78%; float: left; height: auto; margin: 1%;">\n      <ion-item>\n        <ion-label stacked>\n          Tag ID\n        </ion-label>\n        <ion-input #tagidid type="text" [(ngModel)]="tagid" name="tagid" clearInput="true" placeholder="Enter Tag ID" (keyup.enter)="Quantity.setFocus()"></ion-input>\n      </ion-item>\n    </div>\n    <div style="width: 18%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n      <button ion-button color="light" (click)="scanCode(\'tagid\')" style="text-align: center; width: 95%; height: 100%;">\n        <ion-icon name="md-barcode"></ion-icon>\n      </button>\n    </div>\n  </div>\n\n  <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n    <div style="width: 68%; float: left; height: 50px; margin: 1%;">\n      <ion-item>\n        <ion-label style="width: 20%">\n          Tag ID\n        </ion-label>\n        <ion-input #tagidid type="text" [(ngModel)]="tagid" name="tagid" clearInput="true" placeholder="Enter Tag ID here" (keyup.enter)="Quantity.setFocus()"></ion-input>\n      </ion-item>\n    </div>\n    <div style="width: 28%; float: right; height: 50px; padding-right: 2%; margin: 1%;">\n      <button ion-button color="light" (click)="scanCode(\'tagid\')" style="text-align: center; width: 95%; height: 100%;">\n        <ion-icon name="md-barcode"></ion-icon><p>&nbsp; Scan Tag ID</p>\n      </button>\n    </div>\n  </div>\n\n  <!--\n    Quantity\n    Required\n  -->\n\n  <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n    <div style="width: 96%; float: left; height: auto; margin: 1%;">\n      <ion-item>\n        <ion-label stacked>\n          Quantity\n        </ion-label>\n        <ion-input #quantityid type="number" pattern="[0-9]*" [(ngModel)]="quantity" name="quantity" clearInput="true" placeholder="Enter quantity" (keyup.enter)="ExtDesc.setFocus()"></ion-input>\n      </ion-item>\n    </div>\n  </div>\n\n  <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n    <div style="width: 96%; float: left; height: 50px; margin: 1%;">\n      <ion-item>\n        <ion-label style="width: 14.16%">\n          Quantity\n        </ion-label>\n        <ion-input #quantityid type="number" pattern="[0-9]*" [(ngModel)]="quantity" name="quantity" clearInput="true" placeholder="Enter quantity here" (keyup.enter)="ExtDesc.setFocus()"></ion-input>\n      </ion-item>\n    </div>\n  </div>\n\n  <!--\n    Extended Description\n    Not required\n  -->\n\n  <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth > 576">\n      <div style="width: 96%; float: left; height: auto; margin: 1%;">\n        <ion-item>\n          <ion-label stacked>\n            Extended Description\n          </ion-label>\n          <ion-input #extdescid type="text" [(ngModel)]="extdesc" name="extdesc" clearInput="true" placeholder="Enter any additional info"></ion-input>\n        </ion-item>\n      </div>\n    </div>\n  \n    <div style="padding: 2%; vertical-align: middle; height: auto;" [hidden]="devicewidth < 576">\n      <div style="width: 96%; float: left; height: 50px; margin: 1%;">\n        <ion-item>\n          <ion-label style="width: 14.16%">\n            Ext. Descr.\n          </ion-label>\n          <ion-input #extdescid type="text" [(ngModel)]="extdesc" name="extdesc" clearInput="true" placeholder="Enter any additional info"></ion-input>\n        </ion-item>\n      </div>\n    </div>\n\n  <div id="help_button_container">\n    <button ion-button clear style="text-align: center;" (click)="openModal()">\n      Help &nbsp; <ion-icon name="help-circle"></ion-icon>\n    </button>\n  </div>\n</ion-content>\n\n<ion-footer>\n  <div id="add_to_cart_button">\n    <button ion-button block style="text-align: center;" (click)="addToCart()">Add To Cart</button>\n  </div>\n</ion-footer>'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/order/order.html"*/
    }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
], OrderPage);

let OrderInfoModal = class OrderInfoModal {
    constructor(platform, viewCtrl) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
};
OrderInfoModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/order/orderinfomodal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons start>\n            <button ion-button clear ion-start (click)="dismiss()">\n                <span ion-text color="primary" showWhen="ios">Close</span>\n                <ion-icon name="md-close" showWhen="android"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>\n            Order Form Info\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-list>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                Ship to ID\n            </h2>\n            <p>\n                About: It is the ID of the entity to whom item is being shipped.\n            </p>\n            <h4 style="color: #a90000;">\n                <b>Req: It accepts only numbers (digits 0-9). Cannot be longer than 11 digits</b>.\n            </h4>\n        </ion-item>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                Contact ID\n            </h2>\n            <p>\n                About: It is the contact ID of the requesting individual.\n            </p>\n            <h4 style="color: #a90000;">\n                Req: It accepts only numbers (digits 0-9). Cannot be longer than <b>10 digits</b>.\n            </h4>\n        </ion-item>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                PO Number\n            </h2>\n            <p>\n                About: It is the purchase order number.\n            </p>\n            <h4 style="color: #a90000;">\n                Req: It accepts only text.\n            </h4>\n        </ion-item>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                Tag ID\n            </h2>\n            <p>\n                About: It is the item/product identifier.\n            </p>\n            <h4 style="color: #a90000;">\n                <b>Req: It accepts only text. Cannot be blank.</b>\n            </h4>\n        </ion-item>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                Quantity\n            </h2>\n            <p>\n                About: It is the quantity of the particular item.\n            </p>\n            <h4 style="color: #a90000;">\n                <b>Req: It accepts only numbers (digits 0-9). Cannot be blank.</b>\n            </h4>\n        </ion-item>\n        <ion-item text-wrap>\n            <h2 style="padding-top: 2px;">\n                Extended Description\n            </h2>\n            <p>\n                About: It any additional description you wish to provide.\n            </p>\n            <h4 style="color: #a90000;">\n                Req: It accepts only text.\n            </h4>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/order/orderinfomodal.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], OrderInfoModal);

var OrderPage_1;
//# sourceMappingURL=order.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_order__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_global__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







let HomePage = HomePage_1 = class HomePage {
    constructor(navCtrl, loadingCtrl, sqlite, menuCtrl, platform, dataProv) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.sqlite = sqlite;
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.dataProv = dataProv;
        this.orders = Array();
        this.deviceWidth = this.platform.width();
        menuCtrl.swipeEnable(true);
        this.setData();
    }
    setData() {
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        })
            .then((db) => {
            let sql = 'SELECT * FROM orderList ' +
                'WHERE user_id = ? ' +
                'ORDER BY order_id DESC';
            db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_5__app_global__["a" /* Global */].userid])
                .then((result) => {
                for (let i = 0; i < result.rows.length; i++) {
                    let orderitem = [
                        result.rows.item(i).order_id,
                        result.rows.item(i).timestamp,
                        result.rows.item(i).size
                    ];
                    this.orders.push(orderitem);
                }
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    refreshOrderList(refresher) {
        this.dataProv.getAsyncData().then((newData) => {
            /*
            for (var j = 0; j < 100; j++) {
              this.orders.pop();
            }
            for (var i = 0; i < 10; i++) {
              this.orders.push(newData[i]);
            }
            refresher.complete();
            */
        });
    }
    refreshPage() {
        let loading = this.loadingCtrl.create({
            content: 'Refreshing content...'
        });
        loading.present();
        setTimeout(() => {
            this.navCtrl.setRoot(HomePage_1);
            loading.dismiss();
        }, 1000);
    }
    pageRedirect(target) {
        if (target == 'order') {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__order_order__["b" /* OrderPage */]);
        }
    }
};
HomePage = HomePage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n      <span [hidden]="deviceWidth < 576">\n        Menu\n      </span>\n    </button>\n    <ion-title>Past Orders</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-end outline (click)="pageRedirect(\'order\')">\n        &nbsp; New <ion-icon name="add"></ion-icon> &nbsp;\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <!--\n  <ion-refresher (ionRefresh)="refreshOrderList($event)">\n    <ion-refresher-content\n      pulling-icon="arrow-dropdown"\n      pulling-text="Pull to refresh"\n      refreshingSpinner="ios-small"\n      refreshingText="Fetching latest orders">\n    </ion-refresher-content>\n  </ion-refresher>\n-->\n<div id="ion-cards" *ngIf="orders.length > 0">\n  <ion-card *ngFor="let order of orders">\n    <ion-card-header>\n      Order for {{ order[2] }} items confirmed.\n    </ion-card-header>\n    <ion-card-content>\n      {{ order[1] }}\n    </ion-card-content>\n  </ion-card>\n</div>\n<div id="blank-ion-card" *ngIf="orders.length == 0" style="text-align: center;">\n  <p>\n    No completed orders yet.\n  </p>\n</div>\n\n</ion-content>\n\n<ion-footer>\n  <div style="text-align: center;">\n    <button ion-button round (click)="refreshPage()" color="secondary" style="margin: 1%; margin-bottom: 1.3%;">\n      Refresh &nbsp; <ion-icon name="refresh"></ion-icon>\n    </button>\n  </div>\n</ion-footer>\n'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__providers_dataprovider__["a" /* DataProvider */]])
], HomePage);

var HomePage_1;
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Encryption; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

let Encryption = class Encryption {
    constructor() {
        this.hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        /*
        getDec(input: string): number {
    
            let decimal: number = 0;
            let hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
            for(let index = (input.length - 1); index >= 0; index--) {
                decimal = decimal + (this.getPower(16, (input.length - 1 - index)) * hexArray.indexOf(input.charAt(index)));
            }
            return decimal;
    
        }
        */
        /*
        // Not needed
        getDec(input: string) {
            return new Promise((resolve, reject) => {
                let decimal: number = 0;
                for(let i = (input.length - 1); i >= 0; i--) {
                    decimal += ((this.getPower(16, (input.length - 1 - i)) * this.hexArray.indexOf(input.charAt(i))));
                    if(i==0) {
                        resolve(decimal);
                    }
                }
            });
        }*/
        /*
        getHex(input: number): string {
            
            let hex: string = '';
            let hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
            while(input > 0) {
                let remainder: number = input % 16;
                hex = hexArray[remainder] + hex;
                input = input / 16;
                input = parseInt(input.toString());
            }
            return hex;
    
        }
        */
        /*
        // Not needed
        getHex(input: number) {
            return new Promise((resolve, reject) => {
                let hex: string = '';*/
        /*
        let inputCopy = input;
        for(let i = input; i > 0; i = parseInt((inputCopy/16).toString())) {
            hex = this.hexArray[inputCopy%16].toString() + hex;
            //alert(this.hexArray[inputCopy%16].toString());
            inputCopy = parseInt((inputCopy/16).toString());
            //alert('New value of inputCopy ' + inputCopy);
            if((i-1) <= 0) {
                alert('Returning hex' + hex);
                resolve(hex);
            }
        }
        */
        /*
        while(input > 0) {
           hex = this.hexArray[input % 16] + hex;
           input = input/16;
           input = parseInt(input.toString());
           if(input == 0) {
               //alert('Returning hex' + hex);
               resolve(hex);
           }
       }
    });
}
*/
        // Not needed
        /*
        getPower(base: number, exponent: number): number {
    
            let result = 1;
            if(exponent == 0) {
                return result;
            }
            else {
                for(var i = 0; i < exponent; i++) {
                    result *= base;
                    if(i == (exponent - 1)) {
                        return result;
                    }
                }
            }
    
        }*/
    }
    /*
    decryptData(data: string): string {

        let crypt_data = [
            -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
            -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
            -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
            -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
            -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
            -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
            104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
            -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
            43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
            -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
            -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
            39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
            76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
            -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
            -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
            -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
            33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
            -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
            40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
            -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
            103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
            -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
            -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
            2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
            5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
            97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
            -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
            104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
            -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
            -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
            -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
            0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
            -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
            3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
            23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
            14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
            -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
            -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
            46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
            60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
        ];

        if(data != null && data != '' && data != undefined) {
            
            // getDecCode()
            let ddata: string = '';
            for(let index = 0; index < (data.length - 1); index = index + 2) {
                ddata += String.fromCharCode(this.getDec(data.substring(index, index + 2)));
            }

            // getDecryptStageTwo()
            let uddata: string = '';
            for(let index = 0; index < ddata.length; index++) {
                let localvalue = ddata.charCodeAt(index) ^ crypt_data[(index % crypt_data.length)];
                while(localvalue < 0) {
                    localvalue += 256;
                }
                uddata += String.fromCharCode(localvalue);
            }

            data = uddata;
            // Uncompress data

        }

        return data;

    }

    encryptData(data: string): string {

        let crypt_data = [
            -31,15,82,-97,29,-106,-16,-76,-117,-69,-82,106,25,-126,-116,-63,-57,-24,-118,-73,-107,-18,-88,77,-125,
            -109,-26,-125,-111,-38,68,89,-65,-65,-62,-50,13,69,96,-27,123,107,30,-101,9,50,-4,-16,-73,-104,
            -6,-26,-123,-99,24,125,115,68,86,-78,-127,-119,-76,-119,-81,112,50,-2,-3,-11,-48,21,107,27,-114,
            -53,-7,-31,101,33,-119,72,106,22,117,78,-120,-84,99,-13,-58,-29,113,58,36,-72,-97,31,-94,47,
            -19,-91,64,68,91,-53,-6,-6,-23,-111,-36,80,-105,-9,-38,71,101,-2,-8,-36,83,-93,54,18,97,
            -22,-108,-24,-113,-48,18,94,-35,86,-81,117,75,123,110,43,-39,66,76,-128,-121,-88,74,119,85,-82,
            104,13,67,84,-90,67,81,-102,4,24,127,127,-126,-114,-51,6,32,-92,59,43,-34,91,-55,-15,-68,
            -80,119,88,-70,-89,69,93,-40,60,51,4,22,114,65,74,106,73,111,47,-14,-66,-67,-75,-112,-44,
            43,-36,78,-117,-71,-95,44,-31,13,8,41,-41,53,14,72,109,35,-77,-122,-94,49,-7,-28,120,95,
            -33,98,-17,-83,102,0,5,27,-117,-66,-69,-87,82,-100,16,87,-73,-102,6,37,-66,-72,-100,19,99,
            -4,-13,-61,-42,50,1,9,52,9,47,-17,-78,127,125,117,80,-18,-21,-100,14,74,121,98,-22,-96,
            39,-56,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-70,-92,56,31,-97,34,-82,109,38,-64,-60,-37,
            76,126,122,105,17,92,-48,23,120,90,-58,-27,126,120,92,-45,36,-74,-110,-31,106,20,105,15,79,
            -110,-33,93,-43,48,-11,-53,-4,-18,-86,89,-63,-52,1,7,40,-54,-10,-43,46,-24,-115,-61,-44,38,
            -62,-47,26,-124,-104,-1,0,2,15,77,-123,-96,36,-69,-84,94,-37,73,113,60,49,-9,-40,58,38,
            -59,-34,88,-67,-77,-124,-106,-14,-63,-54,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,-50,11,57,
            33,-84,96,-25,-120,-86,86,-75,-114,-56,-19,-93,51,6,15,-79,111,100,-7,-33,95,-30,110,45,-26,
            -128,-123,-101,11,62,59,33,-47,28,-113,-41,55,26,-121,-91,62,56,28,-109,-29,118,82,-95,41,-44,
            40,-49,16,82,-97,29,-106,-16,-75,-117,-69,-82,107,25,-127,-116,-64,-57,-24,-118,-74,-107,-18,-88,76,
            -125,-109,-26,-125,-111,-39,68,89,-65,-65,-62,-50,13,70,96,-28,123,108,30,-101,9,49,-4,-16,-73,
            103,7,42,-41,53,14,72,109,35,-76,-122,-93,49,-7,-28,120,95,-33,98,-18,-83,101,0,4,27,
            -116,-66,-69,-87,81,-100,16,87,-73,-102,6,37,-67,-72,-99,19,100,-10,-46,33,-87,84,-87,79,-112,
            -46,31,-99,22,112,52,11,59,46,-22,-103,2,12,65,71,104,10,55,21,110,40,-51,3,20,102,
            2,17,90,-60,-40,63,63,66,79,-115,-58,-32,100,-5,-20,-98,121,-119,-78,124,113,55,24,122,103,
            5,30,-104,-4,-13,-60,-42,50,1,10,52,9,47,-17,-78,127,125,117,80,-108,-21,-100,14,75,121,
            97,-20,-96,39,-57,-22,-105,-11,-51,8,44,-29,116,70,99,-15,-71,-92,57,31,-96,34,-82,109,38,
            -64,-59,-37,76,126,123,105,17,92,-47,23,120,90,-57,-27,125,120,93,-45,35,-74,-110,-31,106,20,
            104,15,80,-110,-33,93,-43,48,-12,-53,-4,-18,-85,89,-63,-52,1,7,40,-54,-10,-43,45,-24,-115,
            -61,-45,38,-61,-47,25,-124,-104,-1,-1,2,15,77,-122,-96,36,-69,-8,94,-38,73,114,60,49,-9,
            -41,58,38,-59,-34,88,-67,-77,-125,-106,-13,-63,-55,-12,-56,-17,-80,114,62,61,53,16,85,-85,91,
            -95,44,-31,103,8,42,-42,53,13,72,109,35,-77,-122,-94,49,-7,77,120,95,-32,98,-18,-83,101,
            0,36,27,-116,-66,-70,-87,81,-100,17,87,-73,-102,7,37,-66,-72,-100,19,100,-10,-45,33,-87,84,
            -88,79,-112,-46,30,-99,22,112,52,11,59,46,-21,-103,1,12,64,71,104,10,54,21,109,40,-52,
            3,20,102,2,17,89,-60,-39,63,63,66,78,-115,-58,-32,100,-5,-21,-98,27,-119,-79,124,113,55,
            23,122,102,5,29,-104,-3,-13,-60,-42,50,1,9,52,9,47,-17,-78,123,15,117,88,-107,-21,-101,
            14,75,121,98,-20,-95,39,-57,-22,-105,-11,-50,8,15,-29,116,70,99,-15,-70,-92,57,31,-96,34,
            -81,109,38,-64,-60,-37,76,126,123,105,17,92,-47,23,119,90,-58,-27,125,120,92,-45,35,-74,-109,
            -31,105,20,105,15,79,-110,-34,93,-42,48,-12,-53,-4,-18,-85,89,-63,-52,0,7,40,-54,-9,-43,
            46,-24,-116,-61,-45,38,-61,-47,25,-124,-103,-1,-1,2,14,77,-123,-96,37,-69,-84,94,-38,73,114,
            60,48,-9,-41,58,38,-59,-35,88,-67,-77,-124,-106,-13,-63,-54,-12,-56,-17,-80,114,63,61,54,11
        ];

        if(data != null && data != '' && data != undefined) {
            
            // Compress data

            // getEncryptStageOne()
            let cdata: string = '';
            for(let index = 0; index < data.length; index++) {
                let localComputed = data.charCodeAt(index) ^ crypt_data[(index % crypt_data.length)];
                while(localComputed < 0) {
                    localComputed += 256;
                }
                cdata += String.fromCharCode(localComputed);
            }

            // getHexCode()
            let hcdata: string = '';
            for(let index = 0; index < cdata.length; index++) {
                let hex = (this.getHex(cdata.charCodeAt(index))).toUpperCase();
                if(hex.length == 1) {
                    hex = '0' + hex;
                }
                hcdata += hex;
            }
            data = hcdata;
        }

        return data;

    }
    */
    encryptDataAsPromise(data) {
        return new Promise((resolve, reject) => {
            let crypt_data = [
                -31, 15, 82, -97, 29, -106, -16, -76, -117, -69, -82, 106, 25, -126, -116, -63, -57, -24, -118, -73, -107, -18, -88, 77, -125,
                -109, -26, -125, -111, -38, 68, 89, -65, -65, -62, -50, 13, 69, 96, -27, 123, 107, 30, -101, 9, 50, -4, -16, -73, -104,
                -6, -26, -123, -99, 24, 125, 115, 68, 86, -78, -127, -119, -76, -119, -81, 112, 50, -2, -3, -11, -48, 21, 107, 27, -114,
                -53, -7, -31, 101, 33, -119, 72, 106, 22, 117, 78, -120, -84, 99, -13, -58, -29, 113, 58, 36, -72, -97, 31, -94, 47,
                -19, -91, 64, 68, 91, -53, -6, -6, -23, -111, -36, 80, -105, -9, -38, 71, 101, -2, -8, -36, 83, -93, 54, 18, 97,
                -22, -108, -24, -113, -48, 18, 94, -35, 86, -81, 117, 75, 123, 110, 43, -39, 66, 76, -128, -121, -88, 74, 119, 85, -82,
                104, 13, 67, 84, -90, 67, 81, -102, 4, 24, 127, 127, -126, -114, -51, 6, 32, -92, 59, 43, -34, 91, -55, -15, -68,
                -80, 119, 88, -70, -89, 69, 93, -40, 60, 51, 4, 22, 114, 65, 74, 106, 73, 111, 47, -14, -66, -67, -75, -112, -44,
                43, -36, 78, -117, -71, -95, 44, -31, 13, 8, 41, -41, 53, 14, 72, 109, 35, -77, -122, -94, 49, -7, -28, 120, 95,
                -33, 98, -17, -83, 102, 0, 5, 27, -117, -66, -69, -87, 82, -100, 16, 87, -73, -102, 6, 37, -66, -72, -100, 19, 99,
                -4, -13, -61, -42, 50, 1, 9, 52, 9, 47, -17, -78, 127, 125, 117, 80, -18, -21, -100, 14, 74, 121, 98, -22, -96,
                39, -56, -22, -105, -11, -51, 8, 44, -29, 116, 70, 99, -15, -70, -92, 56, 31, -97, 34, -82, 109, 38, -64, -60, -37,
                76, 126, 122, 105, 17, 92, -48, 23, 120, 90, -58, -27, 126, 120, 92, -45, 36, -74, -110, -31, 106, 20, 105, 15, 79,
                -110, -33, 93, -43, 48, -11, -53, -4, -18, -86, 89, -63, -52, 1, 7, 40, -54, -10, -43, 46, -24, -115, -61, -44, 38,
                -62, -47, 26, -124, -104, -1, 0, 2, 15, 77, -123, -96, 36, -69, -84, 94, -37, 73, 113, 60, 49, -9, -40, 58, 38,
                -59, -34, 88, -67, -77, -124, -106, -14, -63, -54, -12, -56, -17, -80, 114, 62, 61, 53, 16, 85, -85, 91, -50, 11, 57,
                33, -84, 96, -25, -120, -86, 86, -75, -114, -56, -19, -93, 51, 6, 15, -79, 111, 100, -7, -33, 95, -30, 110, 45, -26,
                -128, -123, -101, 11, 62, 59, 33, -47, 28, -113, -41, 55, 26, -121, -91, 62, 56, 28, -109, -29, 118, 82, -95, 41, -44,
                40, -49, 16, 82, -97, 29, -106, -16, -75, -117, -69, -82, 107, 25, -127, -116, -64, -57, -24, -118, -74, -107, -18, -88, 76,
                -125, -109, -26, -125, -111, -39, 68, 89, -65, -65, -62, -50, 13, 70, 96, -28, 123, 108, 30, -101, 9, 49, -4, -16, -73,
                103, 7, 42, -41, 53, 14, 72, 109, 35, -76, -122, -93, 49, -7, -28, 120, 95, -33, 98, -18, -83, 101, 0, 4, 27,
                -116, -66, -69, -87, 81, -100, 16, 87, -73, -102, 6, 37, -67, -72, -99, 19, 100, -10, -46, 33, -87, 84, -87, 79, -112,
                -46, 31, -99, 22, 112, 52, 11, 59, 46, -22, -103, 2, 12, 65, 71, 104, 10, 55, 21, 110, 40, -51, 3, 20, 102,
                2, 17, 90, -60, -40, 63, 63, 66, 79, -115, -58, -32, 100, -5, -20, -98, 121, -119, -78, 124, 113, 55, 24, 122, 103,
                5, 30, -104, -4, -13, -60, -42, 50, 1, 10, 52, 9, 47, -17, -78, 127, 125, 117, 80, -108, -21, -100, 14, 75, 121,
                97, -20, -96, 39, -57, -22, -105, -11, -51, 8, 44, -29, 116, 70, 99, -15, -71, -92, 57, 31, -96, 34, -82, 109, 38,
                -64, -59, -37, 76, 126, 123, 105, 17, 92, -47, 23, 120, 90, -57, -27, 125, 120, 93, -45, 35, -74, -110, -31, 106, 20,
                104, 15, 80, -110, -33, 93, -43, 48, -12, -53, -4, -18, -85, 89, -63, -52, 1, 7, 40, -54, -10, -43, 45, -24, -115,
                -61, -45, 38, -61, -47, 25, -124, -104, -1, -1, 2, 15, 77, -122, -96, 36, -69, -8, 94, -38, 73, 114, 60, 49, -9,
                -41, 58, 38, -59, -34, 88, -67, -77, -125, -106, -13, -63, -55, -12, -56, -17, -80, 114, 62, 61, 53, 16, 85, -85, 91,
                -95, 44, -31, 103, 8, 42, -42, 53, 13, 72, 109, 35, -77, -122, -94, 49, -7, 77, 120, 95, -32, 98, -18, -83, 101,
                0, 36, 27, -116, -66, -70, -87, 81, -100, 17, 87, -73, -102, 7, 37, -66, -72, -100, 19, 100, -10, -45, 33, -87, 84,
                -88, 79, -112, -46, 30, -99, 22, 112, 52, 11, 59, 46, -21, -103, 1, 12, 64, 71, 104, 10, 54, 21, 109, 40, -52,
                3, 20, 102, 2, 17, 89, -60, -39, 63, 63, 66, 78, -115, -58, -32, 100, -5, -21, -98, 27, -119, -79, 124, 113, 55,
                23, 122, 102, 5, 29, -104, -3, -13, -60, -42, 50, 1, 9, 52, 9, 47, -17, -78, 123, 15, 117, 88, -107, -21, -101,
                14, 75, 121, 98, -20, -95, 39, -57, -22, -105, -11, -50, 8, 15, -29, 116, 70, 99, -15, -70, -92, 57, 31, -96, 34,
                -81, 109, 38, -64, -60, -37, 76, 126, 123, 105, 17, 92, -47, 23, 119, 90, -58, -27, 125, 120, 92, -45, 35, -74, -109,
                -31, 105, 20, 105, 15, 79, -110, -34, 93, -42, 48, -12, -53, -4, -18, -85, 89, -63, -52, 0, 7, 40, -54, -9, -43,
                46, -24, -116, -61, -45, 38, -61, -47, 25, -124, -103, -1, -1, 2, 14, 77, -123, -96, 37, -69, -84, 94, -38, 73, 114,
                60, 48, -9, -41, 58, 38, -59, -35, 88, -67, -77, -124, -106, -13, -63, -54, -12, -56, -17, -80, 114, 63, 61, 54, 11
            ];
            if (data != null && data != '' && data != undefined) {
                this.getEncryptStageOne(data, crypt_data)
                    .then((cdata) => {
                    this.getHexCode(cdata.toString())
                        .then((hcdata) => {
                        resolve(hcdata.toString());
                    })
                        .catch((e) => {
                        reject(e);
                    });
                })
                    .catch((e) => {
                    reject(e);
                });
            }
            else {
                reject('No data found.');
            }
        });
    }
    decryptDataAsPromise(data) {
        return new Promise((resolve, reject) => {
            let crypt_data = [
                -31, 15, 82, -97, 29, -106, -16, -76, -117, -69, -82, 106, 25, -126, -116, -63, -57, -24, -118, -73, -107, -18, -88, 77, -125,
                -109, -26, -125, -111, -38, 68, 89, -65, -65, -62, -50, 13, 69, 96, -27, 123, 107, 30, -101, 9, 50, -4, -16, -73, -104,
                -6, -26, -123, -99, 24, 125, 115, 68, 86, -78, -127, -119, -76, -119, -81, 112, 50, -2, -3, -11, -48, 21, 107, 27, -114,
                -53, -7, -31, 101, 33, -119, 72, 106, 22, 117, 78, -120, -84, 99, -13, -58, -29, 113, 58, 36, -72, -97, 31, -94, 47,
                -19, -91, 64, 68, 91, -53, -6, -6, -23, -111, -36, 80, -105, -9, -38, 71, 101, -2, -8, -36, 83, -93, 54, 18, 97,
                -22, -108, -24, -113, -48, 18, 94, -35, 86, -81, 117, 75, 123, 110, 43, -39, 66, 76, -128, -121, -88, 74, 119, 85, -82,
                104, 13, 67, 84, -90, 67, 81, -102, 4, 24, 127, 127, -126, -114, -51, 6, 32, -92, 59, 43, -34, 91, -55, -15, -68,
                -80, 119, 88, -70, -89, 69, 93, -40, 60, 51, 4, 22, 114, 65, 74, 106, 73, 111, 47, -14, -66, -67, -75, -112, -44,
                43, -36, 78, -117, -71, -95, 44, -31, 13, 8, 41, -41, 53, 14, 72, 109, 35, -77, -122, -94, 49, -7, -28, 120, 95,
                -33, 98, -17, -83, 102, 0, 5, 27, -117, -66, -69, -87, 82, -100, 16, 87, -73, -102, 6, 37, -66, -72, -100, 19, 99,
                -4, -13, -61, -42, 50, 1, 9, 52, 9, 47, -17, -78, 127, 125, 117, 80, -18, -21, -100, 14, 74, 121, 98, -22, -96,
                39, -56, -22, -105, -11, -51, 8, 44, -29, 116, 70, 99, -15, -70, -92, 56, 31, -97, 34, -82, 109, 38, -64, -60, -37,
                76, 126, 122, 105, 17, 92, -48, 23, 120, 90, -58, -27, 126, 120, 92, -45, 36, -74, -110, -31, 106, 20, 105, 15, 79,
                -110, -33, 93, -43, 48, -11, -53, -4, -18, -86, 89, -63, -52, 1, 7, 40, -54, -10, -43, 46, -24, -115, -61, -44, 38,
                -62, -47, 26, -124, -104, -1, 0, 2, 15, 77, -123, -96, 36, -69, -84, 94, -37, 73, 113, 60, 49, -9, -40, 58, 38,
                -59, -34, 88, -67, -77, -124, -106, -14, -63, -54, -12, -56, -17, -80, 114, 62, 61, 53, 16, 85, -85, 91, -50, 11, 57,
                33, -84, 96, -25, -120, -86, 86, -75, -114, -56, -19, -93, 51, 6, 15, -79, 111, 100, -7, -33, 95, -30, 110, 45, -26,
                -128, -123, -101, 11, 62, 59, 33, -47, 28, -113, -41, 55, 26, -121, -91, 62, 56, 28, -109, -29, 118, 82, -95, 41, -44,
                40, -49, 16, 82, -97, 29, -106, -16, -75, -117, -69, -82, 107, 25, -127, -116, -64, -57, -24, -118, -74, -107, -18, -88, 76,
                -125, -109, -26, -125, -111, -39, 68, 89, -65, -65, -62, -50, 13, 70, 96, -28, 123, 108, 30, -101, 9, 49, -4, -16, -73,
                103, 7, 42, -41, 53, 14, 72, 109, 35, -76, -122, -93, 49, -7, -28, 120, 95, -33, 98, -18, -83, 101, 0, 4, 27,
                -116, -66, -69, -87, 81, -100, 16, 87, -73, -102, 6, 37, -67, -72, -99, 19, 100, -10, -46, 33, -87, 84, -87, 79, -112,
                -46, 31, -99, 22, 112, 52, 11, 59, 46, -22, -103, 2, 12, 65, 71, 104, 10, 55, 21, 110, 40, -51, 3, 20, 102,
                2, 17, 90, -60, -40, 63, 63, 66, 79, -115, -58, -32, 100, -5, -20, -98, 121, -119, -78, 124, 113, 55, 24, 122, 103,
                5, 30, -104, -4, -13, -60, -42, 50, 1, 10, 52, 9, 47, -17, -78, 127, 125, 117, 80, -108, -21, -100, 14, 75, 121,
                97, -20, -96, 39, -57, -22, -105, -11, -51, 8, 44, -29, 116, 70, 99, -15, -71, -92, 57, 31, -96, 34, -82, 109, 38,
                -64, -59, -37, 76, 126, 123, 105, 17, 92, -47, 23, 120, 90, -57, -27, 125, 120, 93, -45, 35, -74, -110, -31, 106, 20,
                104, 15, 80, -110, -33, 93, -43, 48, -12, -53, -4, -18, -85, 89, -63, -52, 1, 7, 40, -54, -10, -43, 45, -24, -115,
                -61, -45, 38, -61, -47, 25, -124, -104, -1, -1, 2, 15, 77, -122, -96, 36, -69, -8, 94, -38, 73, 114, 60, 49, -9,
                -41, 58, 38, -59, -34, 88, -67, -77, -125, -106, -13, -63, -55, -12, -56, -17, -80, 114, 62, 61, 53, 16, 85, -85, 91,
                -95, 44, -31, 103, 8, 42, -42, 53, 13, 72, 109, 35, -77, -122, -94, 49, -7, 77, 120, 95, -32, 98, -18, -83, 101,
                0, 36, 27, -116, -66, -70, -87, 81, -100, 17, 87, -73, -102, 7, 37, -66, -72, -100, 19, 100, -10, -45, 33, -87, 84,
                -88, 79, -112, -46, 30, -99, 22, 112, 52, 11, 59, 46, -21, -103, 1, 12, 64, 71, 104, 10, 54, 21, 109, 40, -52,
                3, 20, 102, 2, 17, 89, -60, -39, 63, 63, 66, 78, -115, -58, -32, 100, -5, -21, -98, 27, -119, -79, 124, 113, 55,
                23, 122, 102, 5, 29, -104, -3, -13, -60, -42, 50, 1, 9, 52, 9, 47, -17, -78, 123, 15, 117, 88, -107, -21, -101,
                14, 75, 121, 98, -20, -95, 39, -57, -22, -105, -11, -50, 8, 15, -29, 116, 70, 99, -15, -70, -92, 57, 31, -96, 34,
                -81, 109, 38, -64, -60, -37, 76, 126, 123, 105, 17, 92, -47, 23, 119, 90, -58, -27, 125, 120, 92, -45, 35, -74, -109,
                -31, 105, 20, 105, 15, 79, -110, -34, 93, -42, 48, -12, -53, -4, -18, -85, 89, -63, -52, 0, 7, 40, -54, -9, -43,
                46, -24, -116, -61, -45, 38, -61, -47, 25, -124, -103, -1, -1, 2, 14, 77, -123, -96, 37, -69, -84, 94, -38, 73, 114,
                60, 48, -9, -41, 58, 38, -59, -35, 88, -67, -77, -124, -106, -13, -63, -54, -12, -56, -17, -80, 114, 63, 61, 54, 11
            ];
            if (data != null && data != '' && data != undefined) {
                console.log('Data found. Initiating process.');
                //alert('Data found. Initiating process.');
                this.getDecCode(data)
                    .then((ddata) => {
                    console.log('Decimal obtained.');
                    //alert('Decimal obtained.');
                    this.getDecryptStageTwo(ddata.toString(), crypt_data)
                        .then((uddata) => {
                        console.log('Decryption complete. Returning promise.');
                        //alert('Decryption complete. Returning promise.');
                        resolve(uddata.toString());
                    })
                        .catch((e) => {
                        reject(e);
                    });
                })
                    .catch((e) => {
                    //alert(e);
                    console.log(e);
                    reject(e);
                });
            }
            else {
                reject('No data found.');
            }
        });
    }
    /*getDecCode(data: string) {
        return new Promise((resolve, reject) => {
            let ddata: string = '';
            for(let i = 0; i < (data.length - 1); i += 2) {
                this.getDec(data.substring(i, i+2))
                .then((dec) => {
                    ddata += String.fromCharCode(Number(dec));
                    if((i+2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
            }
        });
    }*/
    getDecCode(data) {
        return new Promise((resolve, reject) => {
            let ddata = '';
            for (let i = 0; i < (data.length - 1); i += 2) {
                //ddata += String.fromCharCode(parseInt(data.substring(i, i+2), 16));
                /*this.getDec(data.substring(i, i+2))
                .then((dec) => {
                    ddata += String.fromCharCode(Number(dec));
                    if((i+2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                .catch((e) => {
                    reject(e);
                });*/
                let appendingstatement = new Promise((resolve, reject) => {
                    ddata += String.fromCharCode(parseInt(data.substring(i, i + 2), 16));
                    setTimeout(() => {
                        resolve();
                    }, 10);
                })
                    .then(() => {
                    if ((i + 2) >= (data.length - 1)) {
                        resolve(ddata);
                    }
                })
                    .catch((e) => {
                    console.log(e);
                    reject(e);
                });
            }
        });
    }
    getDecryptStageTwo(ddata, crypt_data) {
        return new Promise((resolve, reject) => {
            let uddata = '';
            for (let i = 0; i < ddata.length; i++) {
                uddata += String.fromCharCode((((ddata.charCodeAt(i) ^ crypt_data[(i % crypt_data.length)]) % 256) + 256) % 256);
                if (i == (ddata.length - 1)) {
                    resolve(uddata);
                }
            }
        });
    }
    getEncryptStageOne(data, crypt_data) {
        return new Promise((resolve, reject) => {
            let cdata = '';
            for (let i = 0; i < data.length; i++) {
                cdata += String.fromCharCode((((data.charCodeAt(i) ^ crypt_data[(i % crypt_data.length)]) % 256) + 256) % 256);
                if (i == (data.length - 1)) {
                    resolve(cdata);
                }
            }
        });
    }
    getHexCode(cdata) {
        return new Promise((resolve, reject) => {
            let hcdata = '';
            for (let i = 0; i < cdata.length; i++) {
                //alert((i + 1) + ': ' + cdata.charCodeAt(i));
                /*
                this.getHex(cdata.charCodeAt(i))
                .then((hex) => {
                    //alert(hex.toString());
                    if(hex.toString().length == 1) {
                        let newhex = '0' + hex.toString();
                        hcdata += newhex.toUpperCase();
                        if(i == (cdata.length - 1)) {
                            resolve(hcdata);
                        }
                    }
                    else
                    {
                        hcdata += hex.toString().toUpperCase();
                        if(i == (cdata.length - 1)) {
                            resolve(hcdata);
                        }
                    }
                })
                .catch((e) => {
                    //alert('Error in getting hexCode.');
                    reject(e);
                });
                */
                /*
                let calculatehex = new Promise((resolve, reject) => {
                    let hex = cdata.charCodeAt(i).toString(16).toUpperCase();
                    setTimeout(() => {
                        resolve(hex);
                    }, 10);
                })
                .then((hex) => {
                    if(hex.toString().length == 1) {
                        //hex = '0' + hex;
                        hcdata += '0' + hex;
                    }
                })
                .then(() => {
                    if(i == (cdata.length - 1)) {
                        resolve(hcdata);
                    }
                })
                .catch((e) => {
                    console.log(e);
                    reject(e);
                });
                */
                let hex = cdata.charCodeAt(i).toString(16).toUpperCase();
                if (hex.length == 1) {
                    hcdata += '0' + hex;
                }
                else {
                    hcdata += hex;
                }
                if (i == (cdata.length - 1)) {
                    resolve(hcdata);
                }
            }
        });
    }
};
Encryption = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
], Encryption);

//# sourceMappingURL=cryptprovider.js.map

/***/ }),

/***/ 58:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoginPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorMessageModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_global__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_connectionVariables__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_cryptprovider__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










let LoginPage = class LoginPage {
    constructor(navCtrl, platform, viewCtrl, modalCtrl, toastCtrl, network, sqlite, events, storage, crypt, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.network = network;
        this.sqlite = sqlite;
        this.events = events;
        this.storage = storage;
        this.crypt = crypt;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.username = '';
        this.password = '';
        this.rememberme = false;
        this.failedlogin = false;
        this.deviceheight = this.platform.height();
        this.devicewidth = this.platform.width();
        this.fetchLastValues();
        this.storage.get('userid').then((data) => {
            if (data != null) {
                __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid = data;
                this.checkUserStatus();
            }
        }).catch((e) => {
            console.log('Not present');
        });
    }
    checkUserStatus() {
        let loading = this.loadingCtrl.create({
            content: 'Loading...'
        });
        this.sqlite.create({
            name: __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].db,
            location: 'default'
        })
            .then((db) => {
            let sql = 'SELECT status, lastlogin FROM userProfiles ' +
                'WHERE user_id = ?';
            db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid])
                .then((result) => {
                let userStatus = result.rows.item(0).status;
                if (userStatus == 1) {
                    // Check difference between lastlogin and current timestamp
                    let previous = new Date(result.rows.item(0).lastlogin);
                    let current = new Date();
                    let currentval = current.valueOf();
                    let prevval = previous.valueOf();
                    let difference = currentval - prevval;
                    let differencethreshold = __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].accesslimit * (60 * 1000); // Total value in ms
                    if (difference <= differencethreshold) {
                        // Redirect to home, since already logged in
                        loading.present();
                        this.events.publish('menu:refresh');
                        this.loadLastValues();
                        setTimeout(() => {
                            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                            loading.dismiss();
                        }, 700);
                    }
                    else {
                        console.log('Local storage entry found, but time out.');
                    }
                }
            })
                .catch((e) => {
                console.log(e);
                alert('Issue in reading from userProfiles to check status');
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    createErrorMessage() {
        // Open modal to create and send error message
        let modal = this.modalCtrl.create(ErrorMessageModal);
        modal.present();
    }
    fetchLastValues() {
        this.storage.get('username')
            .then((value) => {
            if (value != null) {
                this.username = value;
                this.storage.get('password')
                    .then((val) => {
                    if (val != null) {
                        this.crypt.decryptDataAsPromise(val)
                            .then((decryptedval) => {
                            this.password = decryptedval.toString();
                            this.rememberme = true;
                        })
                            .catch((e) => {
                            console.log('Error in decryption function call.');
                            console.log(e);
                        });
                    }
                })
                    .catch((e) => {
                    console.log('Error when getting previous password.');
                });
            }
            else {
                this.username = '';
                this.password = '';
                this.rememberme = false;
            }
        })
            .catch((e) => {
            console.log('Error when getting previous username.');
        });
    }
    loadLastValues() {
        this.storage.get('username')
            .then((val) => {
            __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].username = val.toString();
            this.storage.get('email')
                .then((email) => {
                __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].email = email;
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
    logInSubmit() {
        if (this.username == '' || this.username == null || this.username == undefined
            || this.password == '' || this.password == null || this.password == undefined) {
            this.password = '';
            this.rememberme = false;
            alert('Please enter all fields');
            return;
        }
        let loading = this.loadingCtrl.create({
            content: 'Authenticating...'
        });
        let isOnline = this.network.type;
        if (isOnline != 'none') {
            loading.present();
            this.crypt.encryptDataAsPromise(this.username)
                .then((encryptres1) => {
                console.log('Username encryption: ' + encryptres1.toString());
                this.crypt.encryptDataAsPromise(this.password)
                    .then((encryptres2) => {
                    console.log('Password encryption: ' + encryptres2.toString());
                    //let link = 'http://127.0.0.1/airhydrodemo.php';
                    let data = {
                        'action': 'login',
                        'username': encryptres1.toString(),
                        'password': encryptres2.toString()
                    };
                    this.http.setRequestTimeout(__WEBPACK_IMPORTED_MODULE_8__app_connectionVariables__["a" /* ConnectionVariables */].timeout);
                    this.http.acceptAllCerts(true)
                        .then(() => {
                        this.http.post(__WEBPACK_IMPORTED_MODULE_8__app_connectionVariables__["a" /* ConnectionVariables */].httpUrl, data, {})
                            .then((result) => {
                            console.log('Response from server: ' + result.data);
                            this.crypt.decryptDataAsPromise(result.data)
                                .then((decryptval1) => {
                                let res = JSON.parse(decryptval1.toString());
                                if (res.status == 1) {
                                    // User authenticated
                                    this.crypt.decryptDataAsPromise(res.userid)
                                        .then((decryptval2) => {
                                        __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid = parseInt(decryptval2.toString());
                                        this.crypt.decryptDataAsPromise(res.email)
                                            .then((decryptval3) => {
                                            __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].email = decryptval3.toString();
                                            __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].username = this.username;
                                            this.sqlite.create({
                                                name: __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].db,
                                                location: 'default'
                                            })
                                                .then((db) => {
                                                let timestamp = new Date();
                                                let sql = 'INSERT INTO userProfiles VALUES (?, ?, ?, ?, ?)'; // insert userid, username, email, status (set to 1), lastlogin
                                                db.executeSql(sql, [__WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid, this.username, __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].email, 1, timestamp])
                                                    .then(() => {
                                                    this.storage.set('userid', __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid);
                                                    this.setLastValues(__WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].email);
                                                    if (this.rememberme) {
                                                        this.rememberMe();
                                                    }
                                                    console.log('Value inserted succesfully into userProfiles.');
                                                    setTimeout(() => {
                                                        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                                                        loading.dismiss();
                                                    }, 1200);
                                                })
                                                    .catch((e) => {
                                                    console.log(e);
                                                    console.log('Error in inserting value into userProfiles table.');
                                                    sql = 'UPDATE userProfiles ' +
                                                        'SET status = ?, lastlogin = ? ' +
                                                        'WHERE user_id = ?';
                                                    db.executeSql(sql, [1, timestamp, __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid])
                                                        .then(() => {
                                                        this.storage.set('userid', __WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].userid);
                                                        this.resetLastValues();
                                                        this.setLastValues(__WEBPACK_IMPORTED_MODULE_7__app_global__["a" /* Global */].email);
                                                        if (this.rememberme) {
                                                            this.rememberMe();
                                                        }
                                                        console.log('Updated existing entry.');
                                                        setTimeout(() => {
                                                            this.failedlogin = false;
                                                            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                                                            loading.dismiss();
                                                        }, 1200);
                                                    })
                                                        .catch((e) => {
                                                        console.log('Unable to update existing entry.');
                                                        this.failedlogin = true;
                                                        loading.dismiss();
                                                    });
                                                });
                                            })
                                                .catch((e) => {
                                                console.log('Error in connecting to db.');
                                                this.failedlogin = true;
                                                loading.dismiss();
                                            });
                                        })
                                            .catch((e) => {
                                            console.log(e);
                                            this.failedlogin = true;
                                            loading.dismiss();
                                        });
                                    })
                                        .catch((e) => {
                                        console.log(e);
                                        this.failedlogin = true;
                                        loading.dismiss();
                                    });
                                }
                                else {
                                    // User authentication failed
                                    console.log('Error ' + res.status + ': ' + res.error);
                                    this.failedlogin = true;
                                    setTimeout(() => {
                                        this.password = '';
                                        this.failedlogin = true;
                                        this.rememberme = false;
                                        alert('Invalid credentials. Please try again.');
                                        loading.dismiss();
                                    }, 1000);
                                    return;
                                }
                            })
                                .catch((e) => {
                                console.log(e);
                                this.failedlogin = true;
                                loading.dismiss();
                            });
                        })
                            .catch((e) => {
                            let toast = this.toastCtrl.create({
                                message: 'Issue in connecting to server.',
                                duration: 2500,
                                position: 'bottom'
                            });
                            console.log(e);
                            console.log('Issue in backend connection part.');
                            this.failedlogin = true;
                            setTimeout(() => {
                                loading.dismiss();
                                toast.present();
                            }, 200);
                        });
                    })
                        .catch((e) => {
                        let toast = this.toastCtrl.create({
                            message: 'Issue in connecting to server.',
                            duration: 2500,
                            position: 'bottom'
                        });
                        console.log(e);
                        console.log('Issue in backend connection part.');
                        this.failedlogin = true;
                        setTimeout(() => {
                            loading.dismiss();
                            toast.present();
                        }, 200);
                    });
                })
                    .catch((e) => {
                    console.log(e);
                    this.failedlogin = true;
                    loading.dismiss();
                });
            })
                .catch((e) => {
                console.log(e);
                this.failedlogin = true;
                loading.dismiss();
            });
        }
        else {
            let toast = this.toastCtrl.create({
                message: 'No internet connection available. Try again later.',
                showCloseButton: true,
                closeButtonText: 'OK',
                position: 'bottom'
            });
            setTimeout(() => {
                toast.present();
            }, 300);
            return;
        }
    }
    resetLastValues() {
        this.storage.remove('username')
            .catch((e) => {
            console.log('Issue in removing last username.');
        });
        this.storage.remove('password')
            .catch((e) => {
            console.log('Issue in removing last password');
        });
        this.storage.remove('email')
            .catch((e) => {
            console.log('Issue in removing last email');
        });
        this.storage.remove('rememberme')
            .catch((e) => {
            console.log('Issue in removing rememberme');
        });
    }
    rememberMe() {
        this.crypt.encryptDataAsPromise(this.password)
            .then((encryptedval) => {
            this.storage.set('password', encryptedval)
                .then(() => {
                console.log('Password saved to storage.');
                this.storage.set('rememberme', true)
                    .then(() => {
                    console.log('Set rememberme value.');
                })
                    .catch((e) => {
                    console.log('Error in saving rememberme.');
                });
            })
                .catch((e) => {
                console.log('Error in saving password.');
            });
        })
            .catch((e) => {
            console.log('Error in encrypting password.');
        });
    }
    setLastValues(email) {
        this.resetLastValues();
        this.storage.set('username', this.username)
            .then(() => {
            console.log('Username saved in local storage.');
            this.storage.set('email', email)
                .then(() => {
                console.log('Email saved in local storage.');
            })
                .catch((e) => {
                console.log(e);
            });
        })
            .catch((e) => {
            console.log(e);
        });
    }
};
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/login/login.html"*/'<ion-content class="no-scroll" padding>\n  \n    <div showWhen="ios" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">\n      <ion-img style="width: 45%;" src="./assets/imgs/logo.png"></ion-img>\n      <h3>REPAIR DIVISION</h3>\n    </div>\n\n    <div showWhen="android" style="text-align: center; margin-top: 20px; margin-bottom: 20px;">\n      <h2>AIR HYDRO<br>Repair Division</h2>\n    </div>\n\n    <div id="version">\n      <p>v1.0.0 by</p>\n      <ion-img id="companylogo" src="./assets/imgs/developer.png"></ion-img>\n    </div>\n\n    <ion-card [hidden]="deviceheight < 800"> <!-- 190px -->\n      <!--\n      <ion-card-header id="log_in_header">\n        Login\n      </ion-card-header>\n      -->\n      <ion-card-content [hidden]="devicewidth > 576">\n          <ion-item>\n            <ion-label stacked>\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label stacked>\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()" style="margin-top: 10px;">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember me\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n      <ion-card-content [hidden]="devicewidth < 576">\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()" style="margin-top: 10px;">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember username & password\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n    </ion-card>\n  \n    <ion-card [hidden]="deviceheight > 800 || deviceheight < 600"> <!-- 130px -->\n      <!--\n      <ion-card-header id="log_in_header">\n        Login\n      </ion-card-header>\n      -->\n      <ion-card-content [hidden]="devicewidth > 576">\n          <ion-item>\n            <ion-label stacked>\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label stacked>\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()" style="margin-top: 10px;">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember me\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n      <ion-card-content [hidden]="devicewidth < 576">\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()" style="margin-top: 10px;">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember username & password\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n    </ion-card>\n  \n    <ion-card [hidden]="deviceheight > 600"> <!-- 70px -->\n      <!--\n      <ion-card-header id="log_in_header">\n        Login\n      </ion-card-header>\n      -->\n      <ion-card-content [hidden]="devicewidth > 576">\n          <ion-item>\n            <ion-label stacked>\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label stacked>\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember me\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n      <ion-card-content [hidden]="devicewidth < 576">\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Username\n            </ion-label>\n            <ion-input type="text" [(ngModel)]="username" name="username" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label style="width: 20%;">\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" name="password" placeholder="Enter password" autocorrect="off" autocapitalize="none"></ion-input>\n          </ion-item>\n          <div id="login_submit_button">\n            <button ion-button block (click)="logInSubmit()" style="margin-top: 10px;">Log In</button>\n          </div>\n          <ion-item>\n            <ion-label>\n              Remember username & password\n            </ion-label>\n            <ion-toggle [(ngModel)]="rememberme"></ion-toggle>\n          </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n  <div id="contact_us_link" *ngIf="failedlogin" style="text-align: center;">\n    <p style="vertical-align: middle;">Having issues? <button ion-button clear (click)="createErrorMessage()" style="vertical-align: middle;">Let us know</button></p>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/login/login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_9__providers_cryptprovider__["a" /* Encryption */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], LoginPage);

let ErrorMessageModal = class ErrorMessageModal {
    constructor(platform, navCtrl, loadingCtrl, toastCtrl, http, crypt, viewCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.crypt = crypt;
        this.viewCtrl = viewCtrl;
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }
    sendErrorMessage() {
        let loading = this.loadingCtrl.create({
            content: 'Reporting issue...'
        });
        loading.present();
        // Send log report to server
        // Send username, message to server
        //let link = 'http://127.0.0.1/airhydrodemo.php';
        let data = {
            'action': 'reporterror',
            'username': this.modalusername,
            'message': this.description
        };
        console.log('REPERR: STATUS Starting exec -> Sending data to server.');
        this.http.setRequestTimeout(__WEBPACK_IMPORTED_MODULE_8__app_connectionVariables__["a" /* ConnectionVariables */].timeout);
        this.http.acceptAllCerts(true)
            .then(() => {
            this.http.post(__WEBPACK_IMPORTED_MODULE_8__app_connectionVariables__["a" /* ConnectionVariables */].httpUrl, data, {})
                .then((response) => {
                console.log('REPERR: Response from server => ' + response.data);
                this.crypt.decryptDataAsPromise(response.data)
                    .then((decryptedresponse) => {
                    let res = JSON.parse(decryptedresponse.toString());
                    if (res.status == 1) {
                        // Request successful
                        console.log('REPERR: STATUS Ending exec -> Issue reported successfully.');
                        let toast = this.toastCtrl.create({
                            message: 'Issue reported. We will look into it!',
                            duration: 2000,
                            position: 'bottom'
                        });
                        setTimeout(() => {
                            this.viewCtrl.dismiss();
                            loading.dismiss();
                            toast.present();
                        }, 1000);
                    }
                    else if (res.status == 500) {
                        // Internal server error
                        console.log('REPERR: STATUS Ending exec -> Issue not reported.');
                        console.log('REPERR: ERROR -> Error ' + res.status + ': ' + res.error);
                        let toast = this.toastCtrl.create({
                            message: 'Some issue at server. Please try again later.',
                            duration: 2000,
                            position: 'bottom'
                        });
                        setTimeout(() => {
                            loading.dismiss();
                            toast.present();
                        }, 1000);
                    }
                    else {
                        // Insufficient parameters provided
                        console.log('REPERR: STATUS Ending exec -> Issue not reported.');
                        console.log('REPERR: ERROR -> Error ' + res.status + ': ' + res.error);
                        let toast = this.toastCtrl.create({
                            message: 'Unexpected error encountered. Please try again later.',
                            duration: 2000,
                            position: 'bottom'
                        });
                        setTimeout(() => {
                            this.viewCtrl.dismiss();
                            loading.dismiss();
                            toast.present();
                        }, 1000);
                    }
                })
                    .catch((e) => {
                    console.log('REPERR: ERROR -> Error in decrypting info.');
                    console.log('REPERR: A-ERROR -> ' + e);
                    let toast = this.toastCtrl.create({
                        message: 'Unexpected error encountered. Please try again later.',
                        duration: 2000,
                        position: 'bottom'
                    });
                    setTimeout(() => {
                        this.viewCtrl.dismiss();
                        loading.dismiss();
                        toast.present();
                    }, 1000);
                });
            })
                .catch((e) => {
                console.log('REPERR: ERROR -> Error in sending data to server.');
                console.log('REPERR: A-ERROR -> ' + e);
                let toast = this.toastCtrl.create({
                    message: 'Unexpected error encountered. Please try again later.',
                    duration: 2000,
                    position: 'bottom'
                });
                setTimeout(() => {
                    this.viewCtrl.dismiss();
                    loading.dismiss();
                    toast.present();
                }, 1000);
            });
        })
            .catch((e) => {
            console.log('REPERR: ERROR -> Error in sending data to server.');
            console.log('REPERR: A-ERROR -> ' + e);
            let toast = this.toastCtrl.create({
                message: 'Unexpected error encountered. Please try again later.',
                duration: 2000,
                position: 'bottom'
            });
            setTimeout(() => {
                this.viewCtrl.dismiss();
                loading.dismiss();
                toast.present();
            }, 1000);
        });
    }
};
ErrorMessageModal = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/login/errormessagemodal.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons start>\n            <button ion-button clear ion-start (click)="dismiss()">\n                <span ion-text color="primary" showWhen="ios">Close</span>\n                <ion-icon name="md-close" showWhen="android"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>\n            Describe your issue\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-card>\n        <ion-card-header>\n            Username\n        </ion-card-header>\n        <ion-card-content>\n            <ion-input type="text" [(ngModel)]="modalusername" name="modalusername" clearInput="true" placeholder="Enter username" autocorrect="off" autocapitalize="none"></ion-input>\n        </ion-card-content>\n    </ion-card>\n    <ion-card style="white-space: normal; height: auto;">\n        <ion-card-header tyle="white-space: normal; height: auto;">\n            Issue Description\n        </ion-card-header>\n        <ion-card-content style="white-space: normal; height: auto;">\n            <ion-textarea type="text" id="error_description" [(ngModel)]="description" name="description" clearInput="true" placeholder="Tell us more about it" style="white-space: normal; height: auto;"></ion-textarea>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n\n<ion-footer>\n    <div id="report_error_button">\n        <button ion-button block style="text-align: center;" (click)="sendErrorMessage()">Report Issue</button>\n    </div>\n</ion-footer>'/*ion-inline-end:"/Users/tushar/Tushar/AirHydroRD/airhydro_rd/src/pages/login/errormessagemodal.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_http__["a" /* HTTP */],
        __WEBPACK_IMPORTED_MODULE_9__providers_cryptprovider__["a" /* Encryption */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], ErrorMessageModal);

//# sourceMappingURL=login.js.map

/***/ })

},[211]);
//# sourceMappingURL=main.js.map